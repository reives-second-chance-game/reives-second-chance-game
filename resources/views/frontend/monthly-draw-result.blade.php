@extends('layouts.main')

@section('content')
	<div class="container content-monthly-draw-result">	
	   <div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 reeking-cartoon-space d-flex justify-content-center align-items-center" >
				 <img src="{{ asset('/assets/images/reeking_rich.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
				<img src="{{ asset('/assets/images/reeking-rich-large.png') }}" class="mx-auto img-fluid reeking-rich-img-large" alt="Reeking-rich">
				</div>
				<div class="alert d-flex justify-content-center align-items-center text-center mt-1 d-none">					
				</div>	
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center draw-result-section">				  
			    <p class="main-title text-center">
					<span class="lblLine1">ENTER THE QR CODE</span>
					<span class="lblLine2">TO CHECK THE </span>
					<span class="lblLine2">MONTHLY DRAW RESULT </span>				   
				</p>			    
			</div>							
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center draw-result-section" >					
				<div class="row col-12 col-sm-10 col-md-8 col-lg-6 px-1 d-flex justify-content-center align-items-center ">
				<form class="frmManual" id="frmManual">
					 @csrf		
						<div class="mb-4">						  
							<input type="text" class="form-control form-control-lg form-control-alt emailText" id="qrcode" name="qrcode" maxlength="9" placeholder="Enter code" value="">			
                               <span class="text-danger"></span>                                                          
						</div>			 
						<button type="submit" class="btn btn-primary rounded-pill submitQRCode d-flex justify-content-center align-items-center mx-auto" id="submitQRCode">Submit</button>						
					</form>
				</div>											
			</div>		
		</div>
	</div>
@push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script type="text/javascript">
 	var theHeight = $(".page-container").height() + 100;
 	$('#SecondaryContent').height(theHeight);
</script>
<script type="text/javascript">
    $(document).ready(function() {
    
	$('#submitQRCode').on("click",function(e)
	{
		e.preventDefault();
			var error=0;	
			$('.alert').addClass('d-none');
		$('.alert').removeClass('alert-success');
		$('.alert').removeClass('alert-danger');
		$('.alert').html("");	
			
		if($('#qrcode').val() ==""){
			$('.text-danger').html('The QR code is required');
			error=1;
		}		
		/*else if ($('#qrcode').val()!="" && $('#qrcode').val().length>9) {	
			$('.text-danger').html('The QR Code must not be greater than 9 characters.');
			error = 1;
		}*/
		else
		{
			$('.text-danger').html('');
		}
			
		if(error==0){	
            // Serialize the form data
           var form = $('#frmManual').get(0); 
			var formData = new FormData(form);
            // Send an AJAX request
            $.ajax({
                type: 'POST',
                url: '{!! route('qrCodemonthlyDraw.check') !!}',
				processData: false,
				contentType: false,
                data: formData,
                //dataType: 'json',
                success: function(response) 
				{					
					if(response.status=="success")
					{
						$('.draw-result-section').addClass('d-none');
						$('.alert').removeClass('d-none');
						$('.alert').removeClass('alert-danger');
						$('.alert').addClass('alert-success');			
						$('.alert').html(response.message);	
						$('.text-danger').html("");
					}
					else
					{												
						$('.text-danger').html(response.message);
					}				
                },
                error: function(xhr, status, error) {                   
                }
            });
		}	
    });
 });
</script>

@endpush	

@stack('scripts')

@endsection

