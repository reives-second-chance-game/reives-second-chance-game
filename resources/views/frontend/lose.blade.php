@extends('layouts.main')

@section('content')
	<div class="container content-lose-section">	
	   <div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 reeking-cartoon-space  d-flex justify-content-center align-items-center" >
				<div class="skunk-lose-img">
					<img src="{{ asset('/assets/images//skunk-lose.png') }}" class="mx-auto img-fluid skunk-lose-small" alt="image">
					<img src="{{ asset('/assets/images/skunk-lose-large.png') }}" class="mx-auto  img-fluid skunk-lose-large" alt="image">								
				</div> 
			</div>
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center">						
			  <p class="main-title text-center">
				<span>BETTER</span>
				<span >LUCK NEXT </span>
				  <span>TIME </span>
			</p>						
			</div>			
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pt-0 d-flex justify-content-center align-items-center" >
			  <p class="sub-title mb-0">But don't worry... </p>
			</div>
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mt-1 d-flex justify-content-center align-items-center" >
				<p class="chance-to-win-text"> WANT A CHANCE TO WIN £{{env('GAME_AMOUNT'); }}? </p>
			</div>			
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center" >
				<button type="button" class="btn btn-primary rounded-pill monthly-draw-btn" onclick="window.location='{{ url('monthly-draw') }}'">Enter me to the next draw</button>
			</div>										
	  </div>
</div>
@push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
	<script type="text/javascript">	
	 $(document).ready(function() {		 
		 window.addEventListener( "pageshow", function ( event ) {	
     var perfEntries = performance.getEntriesByType("navigation");
	 console.log("previous" + perfEntries);
     if (perfEntries[0].type === "back_forward") {
       location.reload();
     }
    });	 

});
</script>
@endpush	
@stack('scripts')
@endsection