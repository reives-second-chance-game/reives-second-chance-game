@extends('layouts.main')

@section('content')
		   <div class="container-fluid content-section">		
			   <div class="row">
				   <div class="col-md-12 col-lg-12 col-sm-12 col-12 reeking-cartoon-space d-flex justify-content-center align-items-center" >
					   <img src="{{ asset('/assets/images/reeking_rich.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
						<img src="{{ asset('/assets/images/reeking-rich-large.png') }}" class="mx-auto img-fluid reeking-rich-img-large" alt="Reeking-rich">
					</div>					
					<div class="col-md-12 col-lg-12 col-sm-12 col-12 pt-0 sub-title-section d-flex justify-content-center align-items-center" >
					<p class="sub-title game-error text-center">SORRY, THERE WAS A PROBLEM  <br>For further assistance, please contact us at  <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a></p>
					
					</div>							 
				</div>
			</div>
@push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>			
@endpush	

@stack('scripts')			
@endsection