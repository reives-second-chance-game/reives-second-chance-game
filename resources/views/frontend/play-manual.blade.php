@extends('layouts.main')

@section('content')
	<div class="container content-play-manual">	
	   <div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 reeking-cartoon-space d-flex justify-content-center align-items-center" >
				 <img src="{{ asset('/assets/images/reeking_rich.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
				<img src="{{ asset('/assets/images/reeking-rich-large.png') }}" class="mx-auto img-fluid reeking-rich-img-large" alt="Reeking-rich">
				</div>			
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center">				  
			    <p class="main-title text-center">
					<span>ENTER THE CODE</span>
					<span >ON YOUR SCRATCHCARD </span>				  
				</p>			    
			</div>
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center mt-5 alert-msg d-none">
				<p class="alert alert-success"></p>			  
			</div>					
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center emailOptSection" >					
				<div class="row col-12 col-sm-10 col-md-8 col-lg-6 px-1 d-flex justify-content-center align-items-center ">
				<form class="frmManual" id="frmManual">
					 @csrf		
						<div class="mb-4">						  
							<input type="text" class="form-control form-control-lg form-control-alt emailText" id="qrcode" name="qrcode" placeholder="Enter code" value="">			
                               <span class="text-danger"></span>                                                          
						</div>			 
						<button type="submit" class="btn btn-primary rounded-pill submitQRCode d-flex justify-content-center align-items-center mx-auto" id="submitQRCode">Submit</button>						
					</form>
				</div>											
			</div>	
			
	  </div>
	</div>
@push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#submitQRCode').on("click",function(e)
	{
		e.preventDefault();
			var error=0;				
		if($('#qrcode').val() ==""){
			$('.text-danger').html('The QR code is required');
			error=1;
		}		
		/*else if ($('#qrcode').val()!="" && $('#qrcode').val().length>9) {	
			$('.text-danger').html('The QR Code must not be greater than 9 characters.');
			error = 1;
		}*/
		else
		{
			$('.text-danger').html('');
		}
			
		if(error==0){	
            // Serialize the form data
           var form = $('#frmManual').get(0); 
			var formData = new FormData(form);
            // Send an AJAX request
            $.ajax({
                type: 'POST',
                url: '{!! route('qrCodeManual.check') !!}',
				processData: false,
				contentType: false,
                data: formData,
                //dataType: 'json',
                success: function(response) {
					//alert(response.status);
					if(response.status=="success")
					{
						$('.text-danger').html('');
						window.location.href = "{{ url('scratch-card') }}";;						
					}
					else{
												
						$('.text-danger').html(response.message);		
					}				
                },
                error: function(xhr, status, error) {                   
                }
            });
		}	
        });
    });
</script>

@endpush	

@stack('scripts')
@endsection
