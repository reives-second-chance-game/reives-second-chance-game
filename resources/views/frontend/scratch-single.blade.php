@extends('layouts.main')
@section('content')
 <style type="text/css">
.containerScratch
{
    width:25rem;
    height: 21.875rem; 
	border: solid 2px #FFFFFF;
	border-width: 0.3rem;
	background: transparent;
	border-radius: 1rem;
	border-color: #FFF;
    margin:0 auto;
	padding:1.3rem 0.2rem;
	position: relative;
	margin-bottom: 1.5rem;
 }
#promo
{
  width: 98%;
  height: 98%;		  
   position: absolute;
  z-index: 1; 
}

.scratchpad img{
width:100%;
  height: 100%;	
	display:none !important;
	z-index: 0;
	
}

canvas {
   width: 100% !important;
   height: 100% !important;
}	
		
@media (max-width: 500px) {
	  .containerScratch {
	  width: 16rem;
	   height: 15rem;
	   padding:0.5rem 0.2rem;
	 }
	 #promo
	{
	   width: 98%;
	   height: 98%;	
	}
	
	canvas {
	
	}
	
.scratchpad img{
		width: 100% !important;
	   height: 100% !important;
	}
}			
</style>
	<div class="container content-scratch-section">				
	   <div class="row">	 
		@php		
			$imgPath =  (env('ENV_SERVER')=="stack") ? "public/assets/images/scratch/new/" : "assets/images/scratch/new/";
		
			$prizeAmt = Auth::guard('ScannedQrCodes')->user()->prize_amount; 			
			if(Auth::guard('ScannedQrCodes')->user()->win_type==1)
			{
				if(env('ENV_SERVER')=="stack")
					$scratchImage =  base_path($imgPath.'money_'.$prizeAmt.'.png');
				else
					$scratchImage =  public_path($imgPath.'money_'.$prizeAmt.'.png');	
				
				if($prizeAmt>0 && (File::exists($scratchImage))){
					$bgImg = $imgPath.'money_'.$prizeAmt.'.png';									
				}
				else
				{
					$bgImg = "";					
				}		
			}
			else
			{
				$bgImg = $imgPath.'money_loose.png';				
			}		
		@endphp	
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 reeking-cartoon-space d-flex justify-content-center align-items-center" >
				 <img src="{{ asset('/assets/images/reeking_rich.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
				<img src="{{ asset('/assets/images/reeking-rich-large.png') }}" class="mx-auto img-fluid reeking-rich-img-large" alt="Reeking-rich">
				</div>
				<div class="col-md-12 col-lg-12 col-sm-12 col-12 pt-0 sub-title-section d-flex justify-content-center align-items-center alert-msg-section d-none mt-5">
						<p class="alert alert-danger text-center"> </p>					
				</div>				
				 @if(isset($error['status']) && $error['status']=="fail")
					<div class="col-md-12 col-lg-12 col-sm-12 col-12 pt-0 sub-title-section d-flex justify-content-center align-items-center alert-msg mt-5">
						<p class="alert alert-success text-center">{!! $error['message'] !!} </p>					
					</div>	
				 @else
					 <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center  game-block">
							<p class="main-title mb-0">WIN UP TO £{{ env('GAME_AMOUNT'); }}!</p> 			
					</div>							
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pt-0 d-flex justify-content-center align-items-center game-block" >
					  <p class="sub-title">Scratch and match 3 to win </p>
					  <p class="attempts d-none">0</p>	
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pt-0 d-flex justify-content-center align-items-center game-block">
						<!--<img src="images/scratch-bags.png" class="mx-auto small-img">
						<img src="images/scratch-bags-large.png" class="mx-auto large-img">-->
						<div id="containerScratch" class="d-flex justify-content-center align-items-center containerScratch scratch-card-custom">	
							<div id="promo" class="scratchpad"></div>
						 </div>					
					</div>						
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center proceed-btn-section hide">	
						<form class="frmScratch" id="frmScratch">
							@csrf	
							<button type="submit" class="btn btn-primary rounded-pill proceed-btn-custom"><span>PROCEED</span></button>
						</form>	
					</div>	
					@endif
								
		</div>
	</div>		
@push('scripts')

<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('/assets/js/wScratchpad.min.js') }}"></script>
<script src="{{ asset('/assets/js/page-single.js?v=0.5') }}"></script>
<script type="text/javascript">		
	var winType = "{{ Auth::guard('ScannedQrCodes')->user()->win_type }} ";
	var prizeAmt = "{{ Auth::guard('ScannedQrCodes')->user()->prize_amount }} ";	
	var submitURL = "{{ url('scratch-card-status') }}";	
	var postURL = "{{ url('qrcode-status') }}";
	var imgFolder = "<?php echo $imgPath; ?>";	
	var bgImgDesktop = "<?php echo $bgImg; ?>";
	var bgImgMobile =  "<?php echo $bgImg; ?>";	
</script>
<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
</script>	
	
<script type="text/javascript">
$(document).ready(function() {	
	if(navigator.userAgent.indexOf('Mac') > 0){
	}else{	
		console.log(performance.getEntriesByType("navigation")[0].type);
		//console.log('Firefox' + navigator.userAgent.indexOf("Firefox"));	
		if(navigator.userAgent.indexOf("Firefox") != -1 || navigator.userAgent.search("Chrome")>0)
		{
			//console.log(performance.getEntriesByType("navigation")[0].type);
			if(performance.getEntriesByType("navigation")[0].type=="back_forward")
			{
				location.reload(true);
			}
			else{}	  
		}
	}

    $('.proceed-btn-custom').on("click",function(e)
	{
		e.preventDefault();		
		// Serialize the form data
          var form = $('#frmScratch').get(0); 
			var formData = new FormData(form);
            // Send an AJAX request
            $.ajax({
                type: 'POST',
                url: postURL,
				processData: false,
				contentType: false,
                data: formData,
                //dataType: 'json',
                success: function(response) {
					//alert(response.wintype);
					if(response.status=="success")
					{
						if(response.wintype==1)
						{
							window.location.href = "{{ url('win') }}";;	
						}	
						else{
							window.location.href = "{{ url('lose') }}";;	
						}											
					}
					else{						
						
						$('.game-block').addClass('d-none');
						$('.alert-msg-section').removeClass('d-none');
						$('.alert-msg-section .alert-danger').html(response.message);
					//$('.proceed-btn-section').addClass('hide');						
					}				
                },
                error: function(xhr, status, error) {                   
                }
            });
			
        });
		});
</script>
@endpush	

@stack('scripts')

@endsection