<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <meta name="description" content="Reeking Rich - Scratch card game">
	 <meta http-equiv="Cache-Control" content="no-cache, no-store" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Reeking Rich - Scratch game') }}</title>
    <!-- Fonts -->
    <!--<link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">-->	
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])	
	
	<link href="{{ asset('/assets/bootstrap-5.3.3/css/bootstrap.min.css') }}" rel="stylesheet"> 
   <link  href="{{ asset('/assets/css/style.css?v=1.4') }}" rel="stylesheet" type="text/css" media="all" /> 
  
</head>
<body>
    <div id="app">
	<div class="container-fluid page-container m-0 p-0"> 
	<!-- Navigation menubar section starts -->
		<nav class="navbar navbar-custom navbar-expand-lg navbar-light bg-light">
			  <div class="container px-1">
				<a class="navbar-brand"><img src="{{ asset('/assets/images/logo-nav.jpg') }}"  alt="Logo" class="img-fluid"></a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>		
				</button>
				<div class="collapse navbar-collapse" id="navbarText">
				  <ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item d-flex justify-content-center">
				<a class="nav-link d-flex justify-content-center align-items-center active"  aria-current="page" target="_blank" href="https://www.morrisonsfoundation.com/about-us/">About Us</a>
					</li>
					<li class="nav-item d-flex justify-content-center">
					  <a class="nav-link d-flex justify-content-center align-items-center" target="_blank" href="https://www.morrisonsfoundation.com/grant-funding-request/">Grant Funding</a>
					</li>
					<li class="nav-item d-flex justify-content-center ">
					  <a class="nav-link d-flex justify-content-center align-items-center" target="_blank" href="https://www.morrisonsfoundation.com/match-funding-request/">Match Funding</a>
					</li>
				  </ul>
				  <span class="navbar-text">						
						<p class="email-add mb-0"><a href="mailto:foundation.enquiries@morrisonsplc.co.uk">foundation.enquiries@morrisonsplc.co.uk</a></p>
				  </span>
				</div>
			  </div>
		</nav>
		<div class="main-container">
			<section class="background_bg">	
				<!--<main class="py-4">-->
					@yield('content')					
				<!-- Footer section starts -->
				<div id="footer" class="text-center">
					<div class="container text-center">
						<p class="text-muted credit mb-1">Powered By</p>
						<p class="text-muted credit"><img src="{{ asset('/assets/images/logo-footer.png') }}" alt="Powered By" title="Powered By" ></p>
					</div>
				</div>
			<!-- Footer section ends -->
				<!--</main>-->
			</section>
		</div>
	</div>
</div>
<!--@push('scripts')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="{{ asset('/assets/bootstrap-5.3.3/js/bootstrap.bundle.min.js') }}"></script>
@if(Request::segment(1)==='scratch-card')
<script src="{{ asset('/assets/js/wScratchpad.min.js') }}"></script>
<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
</script>	
<script src="{{ asset('/assets/js/page.js') }}"></script>

@endif	
@endpush
@stack('scripts')-->

</body>
</html>
