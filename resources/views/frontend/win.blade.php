@extends('layouts.main')

@section('content')	
	<div class="container-fluid content-won-section">		
			  <!-- <div class="container content-play-section ">-->		
				   <div class="row">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 reeking-cartoon-space  d-flex justify-content-center align-items-center" >
							<div class="skunk-win-img">
								<img src="{{ asset('/assets/images/skunk-win.png') }}" class="mx-auto img-fluid skunk-win-small" alt="image">
								<img src="{{ asset('/assets/images/skunk-win-large.png') }}" class="mx-auto  img-fluid skunk-win-large" alt="image">	
							 </div> 							
						</div>
						 <form class="frmWin" >
							@csrf
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center" >
							  <p class="main-title text-center">
								<span class="won-lbl">YOU WON</span>
								<span class="money-symbol">£{{ Auth::guard('ScannedQrCodes')->user()->prize_amount }}</span>
							  </p> 			
								</div>
							<!--<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pt-0 d-flex justify-content-center align-items-center" >
							  <p class="money-title"> £1 </p>
							</div>-->
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pt-2 d-flex justify-content-center align-items-center" >
								<p class="sub-title"> Redeem your prize at Morrisons<br>using your original scratchcard. </p>
							</div>
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center" >
								<p class="chance-to-win-text"> WANT ANOTHER CHANCE TO WIN? </p>
							</div>			
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center">
								<button type="button" class="btn btn-primary rounded-pill monthly-draw-btn" onclick="window.location='{{ url('monthly-draw') }}'">Enter me to the next draw</button>
							</div>	
						</form>
				  </div>
				</div>	
	@push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>	
	<script type="text/javascript">	
	if (window.performance && window.performance.navigation && window.performance.navigation.type && window.performance.navigation.type == 2) {
		//alert(performance.navigation.type);
		window.location.reload(false);
	}
	 $(document).ready(function() {
	if(performance.navigation.type == 2){	
		location.reload(true);
	}
});
</script>
@endpush	
@stack('scripts')
@endsection