@extends('layouts.main')
@section('content')
<style type="text/css">
	.scratch-card-custom {		
			width:25rem;
		--bs-card-border-width: 0.3rem;
		--bs-card-bg: transparent;
		--bs-card-border-radius: 1rem;
		--bs-card-border-color: #FFF;
		margin-bottom:1.5rem;
		}
		
	.scratch-card-custom .card-body{
		padding: 0.5rem 0.5rem 0rem 0rem;
	}
	
	.scratchpad{
		 width: 100px;
		  height: 120px;
		  position: relative;
		  user-select: none;		 
		}	
		
		.scratchpad:active {
		  transform: scale(1.05);
		}
		
		.scratchpad > img{
			display:none !important;
		}
		
	@media (max-width:470px) {	
	
		.scratch-card-custom {
			width: 19rem !important;
		}
			.scratchpad{
				width: 80px !important;
				height: 90px !important;		  		 
			}			
		}
	
	</style>
	<div class="container content-scratch-section">				
	   <div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 reeking-cartoon-space d-flex justify-content-center align-items-center" >
				 <img src="{{ asset('/assets/images/reeking_rich.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
				<img src="{{ asset('/assets/images/reeking-rich-large.png') }}" class="mx-auto img-fluid reeking-rich-img-large" alt="Reeking-rich">
				</div>
				<div class="col-md-12 col-lg-12 col-sm-12 col-12 pt-0 sub-title-section d-flex justify-content-center align-items-center alert-msg-section d-none mt-5">
						<p class="alert alert-danger text-center"> </p>					
				</div>				
				 @if(isset($error['status']) && $error['status']=="fail")
					<div class="col-md-12 col-lg-12 col-sm-12 col-12 pt-0 sub-title-section d-flex justify-content-center align-items-center alert-msg mt-5">
						<p class="alert alert-success text-center">{{ $error['message'] }} </p>					
					</div>	
				 @else
					 <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center  game-block">
							<p class="main-title mb-0">WIN UP TO £{{ env('GAME_AMOUNT'); }}!</p> 			
					</div>							
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pt-0 d-flex justify-content-center align-items-center game-block" >
					  <p class="sub-title">Scratch off all and match 3 to win! </p>
					  <p class="attempts d-none">0</p>	
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pt-0 d-flex justify-content-center align-items-center game-block">
						<!--<img src="images/scratch-bags.png" class="mx-auto small-img">
						<img src="images/scratch-bags-large.png" class="mx-auto large-img">-->
									<div class="card scratch-card-custom">
								<div class="card-body">
									<div class="row ">
										<div class="col-4 d-flex justify-content-center align-items-center">
											<div id="row1_col_1" class="scratchpad"></div> 
										</div> 
										<div class="col-4  d-flex justify-content-center align-items-center">
											<div id="row1_col_2" class="scratchpad"></div>							
										</div>
										<div class="col-4 d-flex justify-content-center align-items-center">
												<div id="row1_col_3" class="scratchpad"></div>							
										</div>
									</div>
									<div class="row ">
										<div class="col-4 d-flex justify-content-center align-items-center ">
											<div id="row2_col_1" class="scratchpad"></div> 
										</div> 
										 <div class="col-4 d-flex justify-content-center align-items-center ">
											<div id="row2_col_2" class="scratchpad"></div>							
										 </div>
										 <div class="col-4 col-sm-4 d-flex justify-content-center align-items-center ">
												<div id="row2_col_3" class="scratchpad"></div>							
										</div>
									</div>
									<div class="row">
										<div class="col-4 d-flex justify-content-center align-items-center ">
											<div id="row3_col_1" class="scratchpad"></div> 
										</div> 
										 <div class="col-4 d-flex justify-content-center align-items-center">
											<div id="row3_col_2" class="scratchpad"></div>							
										 </div>
										 <div class="col-4 d-flex justify-content-center align-items-center">
												<div id="row3_col_3" class="scratchpad"></div>							
										</div>
									</div>
								</div>									
							</div>
						</div>						
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center proceed-btn-section hide">	
							<form class="frmScratch" id="frmScratch">
								@csrf	
								<button type="submit" class="btn btn-primary rounded-pill proceed-btn-custom"><span>PROCEED</span></button>
							</form>	
						</div>	
					@endif
								
		</div>
	</div>	
@push('scripts')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="{{ asset('/assets/js/wScratchpad.min.js') }}"></script>
<script src="{{ asset('/assets/js/page.js?v=0.5') }}"></script>
<script type="text/javascript">		
		var winType = "{{ Auth::guard('ScannedQrCodes')->user()->win_type }} ";
	var submitURL = "{{ url('scratch-card-status') }}";	
		var postURL = "{{ url('qrcode-status') }}";
</script>
<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
</script>	
	
<script type="text/javascript">
$(document).ready(function() {	
	if(navigator.userAgent.indexOf('Mac') > 0){
	}else{	
		console.log(performance.getEntriesByType("navigation")[0].type);
		//console.log('Firefox' + navigator.userAgent.indexOf("Firefox"));	
		if(navigator.userAgent.indexOf("Firefox") != -1 || navigator.userAgent.search("Chrome")>0)
		{
			//console.log(performance.getEntriesByType("navigation")[0].type);
			if(performance.getEntriesByType("navigation")[0].type=="back_forward")
			{
				location.reload(true);
			}
			else{}	  
		}
	}
	
	  //Use this inside your document ready jQuery 	
	 /* if(performance.navigation.type == 2)
	 {
		$('.game-block').addClass('d-none');
		 $.ajax({
			type: 'POST',
			url: "{{ url('gameStatus') }}",				             
			data: {            
			"_token": "{{ csrf_token() }}"
			},
			success: function(response) 
			{					
				if(response.status=="fail")
				{						
					$('.game-block').addClass('d-none');
					$('.alert-msg-section').removeClass('d-none');
					$('.alert-msg-section .alert-danger').html(response.message);
					$('.proceed-btn-section').addClass('hide');							
				}
				else{					
						$('.game-block').removeClass('d-none');
						$('.alert-msg-section').addClass('d-none');
						$('.alert-msg-section .alert-danger').html("");	
					}				
			},
				error: function(xhr, status, error) {                   
			}
		});
	}*/
    $('.proceed-btn-custom').on("click",function(e)
	{
		e.preventDefault();		
		// Serialize the form data
          var form = $('#frmScratch').get(0); 
			var formData = new FormData(form);
            // Send an AJAX request
            $.ajax({
                type: 'POST',
                url: postURL,
				processData: false,
				contentType: false,
                data: formData,
                //dataType: 'json',
                success: function(response) {
					//alert(response.status);
					if(response.status=="success")
					{
						if(response.wintype==1)
						{
							window.location.href = "{{ url('win') }}";;	
						}	
						else{
							window.location.href = "{{ url('lose') }}";;	
						}											
					}
					else{						
						
						$('.game-block').addClass('d-none');
						$('.alert-msg-section').removeClass('d-none');
						$('.alert-msg-section .alert-danger').html(response.message);
					//$('.proceed-btn-section').addClass('hide');						
					}				
                },
                error: function(xhr, status, error) {                   
                }
            });
			
        });
		});
</script>
@endpush	

@stack('scripts')

@endsection