@extends('layouts.main')

@section('content')
	<div class="container content-emailOpt-section">	
	   <div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 reeking-cartoon-space d-flex justify-content-center align-items-center" >
				 <img src="{{ asset('/assets/images/reeking_rich.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
				<img src="{{ asset('/assets/images/reeking-rich-large.png') }}" class="mx-auto img-fluid reeking-rich-img-large" alt="Reeking-rich">
				</div>
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center">
			  <p class="main-title mb-0">YOUR NUMBER:</p> 			
			</div>
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center">
			  <p class="score-no mb-0">{{ Auth::guard('ScannedQrCodes')->user()->qr_code }}</p>
			</div>
			 @if(isset($error) && $error['status']=="fail")
					@php
					$hideAccessCode = "d-none";
					@endphp
					<div class="col-md-12 col-lg-12 col-sm-12 col-12 pt-0 sub-title-section d-flex justify-content-center align-items-center alert-msg mt-5">
						<p class="alert alert-danger text-center mb-0">{!! $error['message'] !!} </p>					
					</div>	
				 @else
					 @php
					 $hideAccessCode = "";
					@endphp
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center mt-5 alert-msg d-none">
				<p class="alert alert-success text-center"></p>			  
			</div>
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center emailOptSection" >
				<p class="emailme-text"> Email Me If I Have Won! </p>
			</div>			
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 d-flex justify-content-center align-items-center emailOptSection" >					
				<div class="row col-12 col-sm-10 col-md-8 col-lg-6 px-1 d-flex justify-content-center align-items-center ">
				<form class="frmMonthlyDraw" id="frmMonthlyDraw">
					 @csrf		
						<div class="mb-3">						  
							<input type="text" class="form-control form-control-lg form-control-alt emailText" id="email" name="email" placeholder="Email" value="">			
                                <span class="text-danger"></span>                            
						</div>			 
						<button type="submit" class="btn btn-primary rounded-pill btn-emailopt d-flex justify-content-center align-items-center mx-auto">Submit</button>			
					</form>
				</div>											
			</div>	
			@endif
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 pt-0 d-flex justify-content-center align-items-center" >
			  <p class="disclaimer-title px-2 text-center"> 
				<span class="disclaimer-lbl accesswinNos {{ $hideAccessCode }}">Disclaimer</span>
				<span class="disclaimer-lblNxt accesswinNos {{ $hideAccessCode }}"><br>No email? Access winning numbers and draw date <a class="text-reset" href="https://www.rieves.co.uk/monthlywinners" target="_blank">here.</a></span> <br>
				<!--<span class="disclaimer-lblNxt">Contact <a href="mailto:{{ env('ADMIN_MAIL_FROM_ADDRESS') }}" class="remove_link_colour">{{ env('ADMIN_MAIL_FROM_ADDRESS') }}</a> to check if you have won</span> -->
			  </p>
			</div>
	  </div>
	</div>
@push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>	
<script type="text/javascript">
 	var theHeight = $(".page-container").height() + 100;
 	$('#SecondaryContent').height(theHeight);
</script>

<script>
    $(document).ready(function() {
   $('.btn-emailopt').on("click",function(e)
	{
		e.preventDefault();
			var error=0;	
			var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		if($('#email').val() ==""){
			$('.text-danger').html('The email is required');
			error=1;
		}
		else{
			if ($('#email').val()!="" && regex.test($('#email').val())!=true) {	
				$('.text-danger').html('The email is not valid format.');
				error = 1;
			}
			else
			{
				$('.text-danger').html('');
			}
		}	
		if(error==0){	
            // Serialize the form data
           var form = $('#frmMonthlyDraw').get(0); 
			var formData = new FormData(form);
            // Send an AJAX request
            $.ajax({
                type: 'POST',
                url: '{!! route('monthlydraw.store') !!}',
				processData: false,
				contentType: false,
                data: formData,
                //dataType: 'json',
                success: function(response) {
					//alert(response.status);
					if(response.status=="success")
					{
						$('.emailOptSection').addClass('d-none');
						$('.alert-msg').removeClass('d-none');
						$('.alert-success').html(response.message);	
						$('.accesswinNos').addClass('d-none');
					}
					else if(response.status=="emailSubmitted")
					{
						$('.emailOptSection').addClass('d-none');
						$('.alert-msg').removeClass('d-none');
						$('.accesswinNos').addClass('d-none');						
						$('.alert-success').html(response.message);	
					}
					else{
						$('.emailOptSection').removeClass('d-none');
						$('.alert-msg').addClass('d-none');
						$('.alert-success').html("");		
					}					
                },
                error: function(xhr, status, error) {                   
                }
            });
		}	
        });
    });
</script>
@endpush
@stack('scripts')
@endsection