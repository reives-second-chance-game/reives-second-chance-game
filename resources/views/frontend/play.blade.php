@extends('layouts.main')

@section('content')
   <div class="container-fluid content-section content-play-section">		
	   <div class="row">
		   <div class="col-md-12 col-lg-12 col-sm-12 col-12 reeking-cartoon-space d-flex justify-content-center align-items-center" >
			   <img src="{{ asset('/assets/images/reeking_rich.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
				<img src="{{ asset('/assets/images/reeking-rich-large.png') }}" class="mx-auto img-fluid reeking-rich-img-large" alt="Reeking-rich">
			</div>		
			 @if(isset($error) && $error['status']=="fail")
					<div class="col-md-12 col-lg-12 col-sm-12 col-12 pt-0 sub-title-section d-flex justify-content-center align-items-center" >
						<p class="sub-title game-error text-center">{!! $error['message'] !!} </p>					
					</div>	
				 @else
				<form class="frmPlay" action="{{ route('check-qrcode') }}" method="POST">
					 @csrf
					<div class="col-md-12 col-lg-12 col-sm-12 col-12 d-flex justify-content-center align-items-center">
					   <p class="main-title mb-0">GET READY TO PLAY!</p> 			
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 col-12 pt-0 sub-title-section d-flex justify-content-center align-items-center" >
					  <p class="sub-title">Scratch and match 3 to win! </p>				
					</div>	
					<div class="col-md-12 col-lg-12 col-sm-12 col-12 d-flex justify-content-center align-items-center">				
						<button type="submit" class="btn btn-primary rounded-pill play-btn-custom" id="play-btn" ><span>PLAY</span></button>
					</div>	
				</form>
			@endif			
		</div>
	</div>
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
@endsection