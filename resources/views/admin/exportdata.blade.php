@extends('admin.layouts.app_admin')
@section('content')
 <!-- Page Label -->  

<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap-5.3.3/js/bootstrap.bundle.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('/assets/admin/css/jquery-ui.css') }}">
 <script src="{{ asset('/assets/admin/js/oneui.app.min.js') }}"></script>	
<script type="text/javascript" src="{{ asset('/assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
 <script src="{{ asset('/assets/admin/js/jquery-ui.js') }}"></script>
   	<div class="content">
	  <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-start">
			<div class="flex-grow-1">
				<h2 class="fw-semibold text-dark mb-0">Export Data</h2>           
			</div>
			<div class="flex-shrink-0 mt-3 mt-sm-0 ms-sm-3">
				<span class="d-inline-block">                   
				</span>
			</div>
	  </div>
	</div>         
        <!-- END Page Label -->       
        <!-- Page Content -->
        <div class="content exportdataSection">	
			<div class="row">
		@if ($errors->any())
				<div class="alert alert-danger">
					<ul class="mb-0">
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif				
				<!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d-flex align-items-center ">																
					<form class="frmExportData" id="frmExportData" action="{{ url('/admin/exportQRList') }}" method="GET"  >								
					<div class="d-inline-flex align-items-start col-lg-5 col-md-4 col-sm-12 col-xs-12">
						<label for="exampleInputEmail1" class="form-label width-90-px mt-2" >Date From:</label>
						<input type="text" class="form-control" id="dateFrom" name="dateFrom">          
					</div>					
					<div class="d-inline-flex align-items-start col-lg-5 col-md-4 col-sm-12 col-xs-12 ms-md-3">					
						<label for="exampleInputEmail1" class="form-label  width-90-px mt-2 text-nowrap ">Date To:</label>
						<input type="text" class="form-control"  id="dateTo" name="dateTo">          
					</div>
					<div class="d-inline-flex align-items-start col-lg-1 col-md-1 col-sm-12 col-xs-12  pt-1">
						<button type="submit" class="btn btn-primary" id="btn-exportData">Export</button>        
					</div>	
				</form>	
			    </div>	-->	
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d-flex align-items-center ">
				         <!-- Form Horizontal - Default Style -->
                <!-- <form class="frmExportData" id="frmExportData" action="{{ url('/admin/exportQRList') }}" method="GET">-->
				<form class="frmExportData" id="frmExportData">
                    <div class="row mb-3">
                      <label class="col-sm-4 col-form-label" for="example-hf-email">Date From:</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control dateFrom datePicker" id="dateFrom" name="dateFrom" readonly> 
						<span class="datefromErr text-danger"></span>   
                      </div>
                    </div>
                    <div class="row mb-3">
                      <label class="col-sm-4 col-form-label" for="example-hf-password">Date To:</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control dateTo datePicker"  id="dateTo" name="dateTo" readonly> 
						<span class="dateToErr text-danger"></span>   
                      </div>
                    </div>	
					<div class="row mb-2">
						<label class=" ms-md-9 col-md-3 col-12 d-flex justify-content-center align-items-center fw-bold">OR</label>
					</div>	
					<div class="row mb-3">
						<label class="col-sm-4 col-form-label" for="example-hf-password">Game No:</label>
						<div class="col-sm-8">
							<input type="text" class="form-control"  id="gameNo" name="gameNo" maxlength="3" minlength="3"> 
						</div>
                    </div>
                    <div class="row">
						<div class="col-sm-8 ms-auto">
							<button type="submit" class="btn btn-primary" id="btnSubmitExport">Submit</button>  
							<button type="button" class="btn btn-dark mx-2" name="btnCancel" id="btnCancel">Clear</button>
                      </div>
                    </div>
                  </form>
				</div>			
			</div>	
			<div class="row">	
			<div class="block block-rounded">            
				<div class="block-content block-content-custom">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 d-flex ">
					<div class="d-flex justify-content-sm-start align-items-sm-start col-lg-4  col-md-4 col-sm-4 col-4 mb-1 pull-left">
						 <div class="input-group">
							<input class="form-control border searchbox" type="search" placeholder="Search Barcode Number" value="" id="searchbox" name="searchbox">
								<span class="input-group-append">
									<button class="btn btn-outline-secondary bg-white border-bottom-0 border-0 rounded-pill ms-n5" type="button">
										<i class="fa fa-search"></i>
									</button>
								</span>
						</div>					
					</div>	
					<div class="d-flex justify-content-sm-end col-lg-8  col-md-8 col-sm-8 col-8 mb-2 text-right">
						<form class="frmExportCSV" id="frmExportCSV" action="{{ url('/admin/exportQRList') }}" method="GET" >
							<button type="submit" class="btn btn-primary" id="btn-exportData">Export</button>  
						</form>
					</div>	
					</div>
				   <table class="table  table-bordered exportdata-table table-vcenter js-dataTable-full-pagination row-border" cellspacing="0">
					  <thead>
						<tr>
						  <th scope="col" class="d-none">id</th>
						  <th scope="col" class="dt-nowrap">Barcode Number</th>
						   <th scope="col">Game Prize</th>
						   <th scope="col">QR Code</th>
						      <th scope="col">Game No.</th>
							    <th scope="col">Created Date</th>
								 <th scope="col" class="dt-nowrap">Time</th>
								<th scope="col">User Agent</th>	
							 <th scope="col">Audit IP Address</th>
							  <th scope="col">status</th>
						  <th scope="col">Expiry Date</th>                    
						   <th scope="col">Monthly Draw Email</th>  
						  <!--<th scope="col">action</th>-->
						</tr>
						  </thead>
						  <tbody>
						  </tbody>
					</table>
				
				</div>
			  </div>
			  <!-- END Inline --> 		   
		</div>
		</div>
        <!-- END Page Content -->
	@push('scripts')	


<script type="text/javascript">
$(document).ready(function() 
{	
	var dataTable = $('.exportdata-table').DataTable({
      	processData: false,
		contentType: false,
		 processing: true,
          serverSide: true,         
          paging: true,
		  order:3,
		 "responsive": true,
			autoWidth:  false,
		  destroy: true,
		  'iDisplayLength': 10,		
		  	"bInfo" : false,
		    //scrollY:        "100vh",
			scrollX:        true,
	    scrollCollapse: true,
		language: { search: '', searchPlaceholder: "Search...",
			//"sInfo" : "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong> "
			"sInfo" : "",
			},
		'oLanguage': {
			"sEmptyTable": "No data available",	
	  //"oPaginate": { "sPrevious": "", "sNext": ""}, 
	   sLengthMenu: "",
	  "oPaginate": {
		                "sFirst": "",
		                "sPrevious": "<span class='fa fa-angle-left'>",
		                "sNext": "<span class='fa fa-angle-right'>",
		                "sLast": "",
		            }
		},			
        //ajax: "{{ url('admin/exportdata') }}",
		 "ajax": {
					"url": "{{ url('admin/exportdata') }}",
					//"type": "POST",
					"data": function ( d ) {
						//d.searchbox=searchbox, d.dateFrom= dateFrom, d.dateTo= dateTo, d.gameNo=gameNo;   
						//d.searchbox=searchbox;   
						 if($('#dateFrom') && $('#dateFrom').val()!="")
							{
								var dateFrom = $('#dateFrom').val();
							}
							else{
								var dateFrom = "";
							}
							
							 if($('#dateTo') && $('#dateTo').val()!="")
							{
								var dateTo = $('#dateTo').val();
							}
							else{
								var dateTo = "";
							}
						
							 if($('#gameNo') && $('#gameNo').val()!="")
							{
								var gameNo = $('#gameNo').val();
							}
							else{
								var gameNo = "";
							}	
							
							 if($('#searchbox') && $('#searchbox').val()!="")
							{
								var searchbox = $('#searchbox').val();
							}
							else{
								var searchbox = "";
							}
						 d.searchbox= searchbox;
						 d.dateFrom= dateFrom;
						 d.dateTo= dateTo;
						 d.gameNo=gameNo;						
					}
			},
        columns: [
           {data: 'id', name: 'id',  visible: false},
            {data: 'bar_code', name: 'bar_code',"orderable": true, searchable: true, class: "dt-center, dt-nowrap",   sWidth : "50px"},  
			{data: 'prize_amount', name: 'prize_amount', "orderable": false, searchable: true}, 
			{data: 'qr_code', name: 'qr_code',"orderable": true, searchable: true}, 
			{data: 'game_no', name: 'game_no',"orderable": true, searchable: true}, 
			{data: 'created_at', name: 'created_at',"orderable": true, searchable: true}, 
			{data: 'time', name: 'time',"orderable": true, searchable: false, class: "dt-nowrap"}, 
			{data: 'user_agent', name: 'user_agent',"orderable": true, searchable: false}, 
			{data: 'ip_address', name: 'ip_address', "orderable": false, searchable: true},  
			{data: 'final_result', name: 'final_result', "orderable": false, searchable: true},   			
            {data: 'expiry_date', name: 'expiry_date', "orderable": false, searchable: true},  
			{data: 'email', name: 'email', "orderable": false, searchable: true}, 			
           // {data: 'action', name: 'action', orderable: true, searchable: true}
        ],	
		
		"columnDefs": [
			{
				"targets": [-1],
				"className": "text-right",				
			}
		],
		"aoColumnDefs": [{
			/*"aTargets": [0],
			"bSearchable": false,
			"bSortable": false,
			"bVisible": false*/				
		}]
		
    });
	 
 
	$('.datePicker').datepicker({
		  autoclose: true,      
			 autoclose: true,
		  todayHighlight: true,
		  format: "dd/mm/yyyy",
		   altFormat: "dd/mm/yy", 
            dateFormat: 'dd/mm/yy',
		 maxDate: 0,
		  beforeShow: function(){ 
			$(".ui-datepicker").css('font-size', '95%');
			$(".ui-datepicker").css('width', '17em');
		}
	
	});

		 
$("#searchbox").on("keyup search input paste cut", function() {	
		//dataTable.search(this.value).draw();
		  dataTable.draw();		
}); 
	
	 
	$('#btnSubmitExport').on("click",function(e)
	{
			e.preventDefault();
			var error=0;		
		if($('#gameNo').val()==""){
			if($('.dateFrom').val() ==""){
				$('.datefromErr').html('The Date From field is required.');
				error=1;
			}	
			else
			{
				$('.datefromErr').html('');
			}
			if($('.dateTo').val() ==""){					
				$('.dateToErr').html('The Date To field is required.');
				error=1;
			}	
			else
			{
				$('.dateToErr').html('');
			}	
			if(($('.dateFrom').val().length>0 && $('.dateTo').val().length>0))	
			{
				if(process($('.dateFrom').val()) > process($('.dateTo').val())){				
					$('.dateToErr').html('Date To must be greater than Date From');
					$('.dateToErr').addClass('text-nowrap');
					error=1;					
				}
				else
				{
					$('.dateToErr').html('');
					$('.dateToErr').removeClass('text-nowrap');
				}
			}			
		}
		else{
				$('.datefromErr').html('');
				$('.dateToErr').html('');
		}	
			if(error==0){
				//loadExportData();
				 dataTable.draw();
			}
	 });
	 
	$('#btnCancel').on("click",function(e)
	{
		e.preventDefault();
		$('#dateFrom').val("");
		$('#dateTo').val("");
		$('#gameNo').val("");
		$('.datefromErr').html('');
		$('.dateToErr').html('');
		$('#searchbox').val("");
		//loadExportData();
		 dataTable.draw();
			
			
	 });	 
	$('.dataTables_filter input').addClass('form-control');


 $("#frmExportCSV").submit( function(e) {
	 //e.preventDefault();
      $("<input />").attr("type", "hidden")
          .attr("name", "searchbox")
          .attr("value", $('#searchbox').val())
          .appendTo("#frmExportCSV");
		  
		$("<input />").attr("type", "hidden")
          .attr("name", "dateFrom")
          .attr("value", $('#dateFrom').val())
          .appendTo("#frmExportCSV");  
		  
		  	$("<input />").attr("type", "hidden")
          .attr("name", "dateTo")
          .attr("value", $('#dateTo').val())
          .appendTo("#frmExportCSV");  
		  
		$("<input />").attr("type", "hidden")
          .attr("name", "gameNo")
          .attr("value", $('#gameNo').val())
          .appendTo("#frmExportCSV");
		  
		  
		$("<input />").attr("type", "hidden")
          .attr("name", "searchbox")
          .attr("value", $('#searchbox').val())
          .appendTo("#frmExportCSV");		  
		  
      return true;
  });
  
  });
  
  $(document).on( "change", $('.datepicker'), function() {	 
$(this).datepicker('hide');
});

function process(date){
   var parts = date.split("/");
   var date = new Date(parts[1] + "/" + parts[0] + "/" + parts[2]);
   return date.getTime();
}


 /*function loadExportData(){	
	   if($('#dateFrom') && $('#dateFrom').val()!="")
		{
			var dateFrom = $('#dateFrom').val();
		}
		else{
			var dateFrom = "";
		}
		
		 if($('#dateTo') && $('#dateTo').val()!="")
		{
			var dateTo = $('#dateTo').val();
		}
		else{
			var dateTo = "";
		}
	
		 if($('#gameNo') && $('#gameNo').val()!="")
		{
			var gameNo = $('#gameNo').val();
		}
		else{
			var gameNo = "";
		}	
		
		 if($('#searchbox') && $('#searchbox').val()!="")
		{
			var searchbox = $('#searchbox').val();
		}
		else{
			var searchbox = "";
		}
	
		var dataTable = $('.exportdata-table').DataTable({
      	processData: false,
		contentType: false,
		 "responsive": true,
		 autoWidth:         false,
		  destroy: true,
		  'iDisplayLength': 10,
		    //scrollY:        "100vh",
			scrollX:        true,
	    scrollCollapse: true,
		language: { search: '', searchPlaceholder: "Search...",
			//"sInfo" : "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong> "
			"sInfo" : "",
			},
		'oLanguage': { 
	  //"oPaginate": { "sPrevious": "", "sNext": ""}, 
	   sLengthMenu: "",
	  "oPaginate": {
		                "sFirst": "",
		                "sPrevious": "<span class='fa fa-angle-left'>",
		                "sNext": "<span class='fa fa-angle-right'>",
		                "sLast": "",
		            }
		},			
        //ajax: "{{ url('admin/exportdata') }}",
		 "ajax": {
					"url": "{{ url('admin/exportdata') }}",
					//"type": "POST",
					"data": function ( d ) {
						d.searchbox=searchbox, d.dateFrom= dateFrom, d.dateTo= dateTo, d.gameNo=gameNo;                                        
					}
			},
        columns: [
           // {data: 'id', name: 'id',  visible: false},
            {data: 'bar_code', name: 'bar_code',"orderable": true, searchable: true, class: "dt-center, dt-nowrap",   sWidth : "50px"},  
			{data: 'prize_amount', name: 'prize_amount', "orderable": false, searchable: true}, 
			{data: 'qr_code', name: 'qr_code',"orderable": true, searchable: true}, 
			{data: 'game_no', name: 'game_no',"orderable": true, searchable: true}, 
			{data: 'created_at', name: 'created_at',"orderable": true, searchable: true}, 
			{data: 'ip_address', name: 'ip_address', "orderable": false, searchable: true},  
			{data: 'final_result', name: 'final_result', "orderable": false, searchable: true},   			
            {data: 'expiry_date', name: 'expiry_date', "orderable": false, searchable: true},  
			{data: 'email', name: 'email', "orderable": false, searchable: true}, 			
           // {data: 'action', name: 'action', orderable: true, searchable: true}
        ],	
		
		"columnDefs": [
			{
				"targets": [-1],
				"className": "text-right",				
			}
		],
		"aoColumnDefs": [{						
		}]
		
    }); 
	
}*/


</script>
@endpush
@stack('scripts')
    @endsection