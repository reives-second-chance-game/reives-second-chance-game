@extends('admin.layouts.app_guest')
@section('content')
  <div class="bg-body-extra-light">
     <div class="content content-full">
        <div class="row g-0 justify-content-center">
             <div class="col-md-8 col-lg-8 col-xl-8 py-4 px-4 px-lg-5">
				<div class="card py-2 d-flex justify-content-center align-items-center">		
                    <p class="mb-5">
                        <img src="{{ asset('/assets/images/foundation_logo.svg') }}">
                    </p>
					<div class="card-header border-0 bg-body-extra-light h5">{{ __('Forgot Password') }}</div>
					<div class="card-body py-0 border-0">
						@if (session('message'))
							<div class="alert alert-success" role="alert">
								{{ session('message') }}
							</div>
						@endif	
						<form method="POST" action="{{ route('forget.password.post') }}">
							@csrf
							<div class="mb-3 ">													
								<input id="email" type="text" class="form-control" name="email" placeholder="Email Address" value="{{ old('email') }}">
									@if ($errors->has('email'))
									<span class="text-danger">{{ $errors->first('email') }}</span>
								@endif    
							</div>
							<div class="row mb-0">
								<div class="col-md-12 d-flex justify-content-center align-items-center">
									<button type="submit" class="btn btn-primary">
										{{ __('Submit') }}
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@if (session()->has('message'))
    <script>
       setTimeout(function() {
           window.location.replace("/admin/login");
       }, 3000); // 2 second
    </script>
@endif