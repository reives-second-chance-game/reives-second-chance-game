@extends('admin.layouts.app_guest')
@section('content')
 <div class="bg-body-extra-light">
    <div class="content content-full">
        <div class="row g-0 justify-content-center">
            <div class="col-md-8 col-lg-8 col-xl-8 py-4 px-4 px-lg-5">
				<div class="card py-2 d-flex justify-content-center align-items-center">		
                    <p class="mb-5">
                        <img src="{{ asset('/assets/images/foundation_logo.svg') }}">
                    </p>           
					<div class="card-header border-0 bg-body-extra-light h5">{{ __('Reset Password') }}</div>
					<div class="card-body border-0">
						 @if (Session::has('message'))
							 <div class="alert alert-success" role="alert">
								{{ Session::get('message') }}
							</div>
						@endif					
						 @if (Session::has('error'))
							 <div class="alert alert-danger" role="alert">
								{{ Session::get('error') }}
							</div>
						@endif
						<form method="POST" action="{{ route('reset.password.update') }}">
							@csrf
							<input type="hidden" name="token" value="{{ $token }}">
							<div class="row mb-3">                          
								<div class="col-md-12">
									<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"  placeholder="Enter Email Address" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
									@error('email')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>
							<div class="row mb-3">                          
								<div class="col-md-12">
									<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  placeholder="Enter New Password" required autocomplete="new-password">

									@error('password')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
								</div>
							</div>
							<div class="row mb-3">                
								<div class="col-md-12">
									<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm New Password" required autocomplete="new-password">
								</div>
							</div>
							<div class="row mb-0">
								 <div class="col-md-12 d-flex justify-content-center align-items-center">
									<button type="submit" class="btn btn-primary">
										{{ __('Submit') }}
									</button>
								</div>
							</div>
						</form>	
					</div>
					
				</div>
			</div>
        </div>
    </div>
</div>
@endsection
