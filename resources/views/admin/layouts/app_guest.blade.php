<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <meta name="description" content="Reeking Rich - Scratch card game">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Reeking Rich - Scratch game') }}</title>
	 <meta name="author" content="">
    <meta name="robots" content="">
    <!-- Open Graph Meta -->
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
	  <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="assets/media/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/media/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/media/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->
    <!-- Fonts -->
    <!--<link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">-->
	
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
	<link href="{{ asset('/assets/bootstrap-5.3.3/css/bootstrap.min.css') }}" rel="stylesheet"> 
      <!-- Stylesheets -->
    <!-- OneUI framework -->
    <link rel="stylesheet" id="css-main" href="{{ asset('/assets/admin/css/oneui.min.css') }}">
	<link rel="stylesheet" id="css-main" href="{{ asset('/assets/admin/css/admin.css') }}">
    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/amethyst.min.css"> -->
    <!-- END Stylesheets -->
</head>
<body>
     <!-- Main Container -->
      <main id="main-container">
        <!-- Page Content -->
        <div class="d-flex align-items-center">
          <div class="w-100">
					@yield('content')					
			 <!-- Footer -->
            <div class="fs-sm text-center text-muted py-3">
              <a class="fw-semibold" href="#">Copyright</a> &copy; <span data-toggle="year-copy"></span>
			  Rieves All rights reserved
            </div>
            <!-- END Footer -->
          </div>
        </div>
        <!-- END Page Content -->
      </main>
      <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</body>
</html>
@push('scripts')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="{{ asset('/assets/bootstrap-5.3.3/js/bootstrap.bundle.min.js') }}"></script>
  <!--
        OneUI JS
        Core libraries and functionality
        webpack is putting everything together at assets/_js/main/app.js
    -->
    <script src="{{ asset('/assets/admin/js/oneui.app.min.js') }}"></script>
    <!-- jQuery (required for jQuery Validation plugin) -->
    <script src="{{ asset('/assets/admin/js/lib/jquery.min.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('/assets/admin/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
@endpush