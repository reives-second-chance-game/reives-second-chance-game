<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <meta name="description" content="Reeking Rich - Scratch card game">
		<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Reeking Rich - Scratch game') }}</title>
	 <meta name="author" content="">
    <meta name="robots" content="">
    <!-- Open Graph Meta -->
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	  <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="">
    <link rel="icon" type="image/png" sizes="192x192" href="">
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <!-- END Icons --> 
	 @livewireStyles
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])	
      <!-- Stylesheets -->
	  <link href="{{ asset('resources/css/app.css') }}" rel="stylesheet" type="text/css" >
	   <link href="{{ asset('/assets/bootstrap-5.3.3/css/bootstrap.min.css') }}" rel="stylesheet"> 
    <!-- OneUI framework -->
    <link rel="stylesheet" id="css-main" href="{{ asset('/assets/admin/css/oneui.min.css') }}">
	<link rel="stylesheet" id="css-main" href="{{ asset('/assets/admin/css/admin.css') }}">
	<link rel="stylesheet" id="css-main" href="{{ asset('/assets/admin/css/datatables.min.css') }}">   
    <!-- END Stylesheets -->
	
</head>
<body>
  <div id="app">
         <!-- Page Container -->
        <div id="page-container" class="sidebar-o sidebar-dark enable-page-overlay side-scroll page-header-fixed main-content-narrow">
      <nav id="sidebar" aria-label="Main Navigation">
        <!-- Side Header -->
        <div class="content-header">
          <!-- Logo -->
          <a class="fw-semibold text-dual" href="{{ route('dashboard') }}">
            <span class="smini-visible">
              <i class="fa fa-circle-notch text-primary"></i>
            </span>
            <span class="smini-hide fs-5 tracking-wider">  <img src="{{ asset('/assets/images/foundation_logo.svg') }}"></span>
          </a>         
          <!-- END Logo -->

          <!-- Extra -->
          <div>      
			<!-- Close Sidebar, Visible only on mobile screens -->
            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
            <a class="d-lg-none btn btn-sm btn-alt-secondary ms-1" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
              <i class="fa fa-fw fa-times"></i>
            </a>
            <!-- END Close Sidebar -->
          </div>
          <!-- END Extra -->
        </div>
        <!-- END Side Header -->
     
          <!-- Sidebar Scrolling -->
        <div class="js-sidebar-scroll sidebar-dark">
          <!-- Side Navigation -->
          <div class="content-side">
            <ul class="nav-main">
              <li class="nav-main-item">
                <a class="nav-main-link" href="{{ route('dashboard') }}">
                  <i class="nav-main-link-icon si si-speedometer"></i>
                  <span class="nav-main-link-name">Dashboard</span>
                </a>
              </li>
			   <li class="nav-main-item">
                <a class="nav-main-link" href="{{ route('qrcode-list') }}">
                 <i class="fa fa-upload fa-1" aria-hidden="true"></i>
                  <span class="px-2 nav-main-link-name">Upload CSV</span>
                </a>
              </li>
			   <li class="nav-main-item">
                <a class="nav-main-link" href="{{ route('auditreport') }}">
                 <i class="nav-main-link-icon si si-bag"></i>
                  <span class="nav-main-link-name">Audit Report</span>
                </a>
              </li>
			   <li class="nav-main-item">
                <a class="nav-main-link" href="{{ route('exportdata') }}">
                 <i class="nav-main-link-icon si si-bag"></i>
					<span class="nav-main-link-name">Export Data</span>
                </a>
              </li>
			   <!--<li class="nav-main-item">
                <a class="nav-main-link" href="{{ route('monthly-draw-winners') }}">
                 <i class="nav-main-link-icon si si-bag"></i>
                  <span class="nav-main-link-name">Monthly Draw Winners</span>
                </a>
              </li>-->           
            </ul>
          </div>
          <!-- END Side Navigation -->
        </div>
        <!-- END Sidebar Scrolling -->
      </nav>
      <!-- END Sidebar -->

      <!-- Header -->
      <header id="page-header">
        <!-- Header Content -->
        <div class="content-header">
          <!-- Left Section -->
          <div class="d-flex align-items-center">
            <!-- Toggle Sidebar -->
            <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
            <button type="button" class="btn btn-sm btn-alt-secondary me-2 d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
              <i class="fa fa-fw fa-bars"></i>
            </button>
            <!-- END Toggle Sidebar -->

            <!-- Toggle Mini Sidebar -->
            <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
            <button type="button" class="btn btn-sm btn-alt-secondary me-2 d-none d-lg-inline-block" data-toggle="layout" data-action="sidebar_mini_toggle">
              <i class="fa fa-fw fa-ellipsis-v"></i>
            </button>
            <!-- END Toggle Mini Sidebar -->        
          </div>
          <!-- END Left Section -->
          <!-- Right Section -->
          <div class="d-flex align-items-center">     
	       	<ul class="nav-header pull-right pt-2 list-unstyled">		
			   <li class="nav-header-title header-title-effect">					                                 		   
					<a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ route('admin.logout') }}">
						<span class="fs-sm fw-medium">Log out <i class="ps-2 si si-logout"></i> </span>
					</a>						
				</li> 					            
				</ul> 					
          </div>
          <!-- END Right Section -->
        </div>
        <!-- END Header Content -->
        <!-- Header Search -->
         <div id="page-header-search" class="overlay-header bg-body-extra-light">
          <div class="content-header">
            <form class="w-100" action="be_pages_generic_search.html" method="POST">
              <div class="input-group">
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
               <button type="button" class="btn btn-alt-danger" data-toggle="layout" data-action="header_search_off">
                  <i class="fa fa-fw fa-times-circle"></i>
                </button>
                <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
              </div>
            </form>
          </div>
        </div>
        <!-- END Header Search -->

        <!-- Header Loader -->
        <!-- Please check out the Loaders page under Components category to see examples of showing/hiding it -->
        <div id="page-header-loader" class="overlay-header bg-body-extra-light">
          <div class="content-header">
            <div class="w-100 text-center">
              <i class="fa fa-fw fa-circle-notch fa-spin"></i>
            </div>
          </div>
        </div>
        <!-- END Header Loader -->
      </header>
      <!-- END Header -->

      <!-- Main Container -->

      <!-- Main Container -->
      <main id="main-container">
        <!-- Page Content -->
		@yield('content')					
			
        </main>
      <!-- END Main Container -->

      <!-- Footer -->
      <footer id="page-footer" class="bg-body-light">
        <div class="content py-3">
          <div class="row fs-sm">          
            <div class="col-sm-12 order-sm-1 py-1 text-center text-sm-center">
              <a class="fw-semibold" href="#">Copyright</a> &copy; <span data-toggle="year-copy"></span>
			  Rieves All rights reserved
            </div>
          </div>
        </div>
      </footer>
      <!-- END Footer -->
    </div>
    <!-- END Page Container -->
	</div>
@livewireScripts    
  </body>
</html>