 @extends('admin.layouts.app_admin')
@section('content')  
    <div class="content content-full">
	  <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-start">
		<div class="flex-grow-1">
		  <h2 class="fw-semibold text-dark mb-0">Dashboard</h2>           
		</div>
		<div class="flex-shrink-0 mt-3 mt-sm-0 ms-sm-3">
		  <span class="d-inline-block">
		   
		  </span>
		</div>
	  </div>
	</div>       
    <!-- Page Content -->
          <!-- Page Content -->
    <div class="content dashboard-content">
    <!-- Referred Members -->
	@if(count($gameStat)>0)
	@foreach($gameStat as $key=>$stat)
		
          <div class="block block-rounded">
            <div class="block-header block-header-default">
              <h3 class="block-title">Game No:  {{ $stat->game_no }}</h3>
            </div>
            <div class="block-content">
              <div class="row items-push">
                <div class="col-md-3">
                  <!-- Referred User -->
                  <a class="block block-rounded block-bordered block-link-shadow h-100 mb-0 pe-none" href="#">
                    <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                      <div>
                        <div class="fw-semibold fs-sm mb-1">Scanned Entries</div>
                        <div class="fs-sm text-muted">{{ (!empty($stat->totalScanned)) ? $stat->totalScanned : 0 }} </div>
                      </div>
                      <div class="ms-3">
                       <i class="far fa-user-circle fs-3 text-primary"></i>
                      </div>
                    </div>
                  </a>
                  <!-- END Referred User -->
                </div>
                <div class="col-md-3">
                  <!-- Referred User -->
                  <a class="block block-rounded block-bordered block-link-shadow h-100 mb-0 pe-none" href="#">
                    <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                      <div>
                        <div class="fw-semibold fs-sm mb-1">Manual Entries</div>
                        <div class="fs-sm text-muted">{{ (!empty($stat->totalManual)) ? $stat->totalManual : 0 }} </div>
                      </div>
                      <div class="ms-3">
                        <i class="far fa-user-circle fs-3 text-primary"></i>
                      </div>
                    </div>
                  </a>
                  <!-- END Referred User -->
                </div>
                <div class="col-md-3">
                  <!-- Referred User -->
                  <a class="block block-rounded block-bordered block-link-shadow h-100 mb-0 pe-none" href="#">
                    <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                      <div>
                        <div class="fw-semibold fs-sm mb-1">Winners</div>
                        <div class="fs-sm text-muted">{{ (!empty($stat->winCnt)) ? $stat->winCnt : 0 }} </div>
                      </div>
                      <div class="ms-3">
                         <img src="{{ asset('/assets/images/win.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
                      </div>
                    </div>
                  </a>
                  <!-- END Referred User -->
                </div>
				
				    <div class="col-md-3">
                  <!-- Referred User -->
                  <a class="block block-rounded block-bordered block-link-shadow h-100 mb-0 pe-none" href="#">
                    <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                      <div>
                        <div class="fw-semibold fs-sm mb-1">Losers</div>
                        <div class="fs-sm text-muted">{{ (!empty($stat->lostCnt)) ? $stat->lostCnt : 0 }} </div>
                      </div>
                      <div class="ms-3">
                        <img src="{{ asset('/assets/images/lose.png') }}" class="mx-auto img-fluid reeking-rich-img-small" alt="Reeking-rich">
                      </div>
                    </div>
                  </a>
                  <!-- END Referred User -->
                </div>
              </div>
            </div>
          </div>
		  @endforeach
		  @endif
          <!-- END Referred Members -->
   		
 <!-- END Page Content -->
 @push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap-5.3.3/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('/assets/admin/js/oneui.app.min.js') }}"></script>	
@endpush
@stack('scripts')
@endsection