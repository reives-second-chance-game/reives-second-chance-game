@extends('admin.layouts.app_admin')
@section('content')
<link rel="stylesheet" 	<link rel="stylesheet" id="css-main" href="{{ asset('/assets/admin/css/jquery-ui.css') }}"> 
	     <!-- Hero -->       
            <div class="content ">
              <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-start">
                <div class="flex-grow-1">
                  <h2 class="fw-semibold text-dark mb-0">Audit Report</h2>           
                </div>
                <div class="flex-shrink-0 mt-3 mt-sm-0 ms-sm-3">
                  <span class="d-inline-block">                   
                  </span>
                </div>
              </div>
            </div>         
        <!-- END Hero -->       
        <!-- Page Content -->
        <div class="content">	
			<div class="row">						
				<div class="row col-md-12 mb-3 ">		
							<label for="exampleInputEmail1" class="form-label col-md-2 mt-2  pe-0">Barcode Number:</label>
							<div class="col-md-5 ">
							<div class="form-material form-material-success input-group free-text-search">
							<input type="text" class="form-control searchBarCode" placeholder="Barcode Number" id="searchBarCode" name="searchBarCode" value=""/>		
							<span class="input-group-addon nav-main-header-search-magnifier cursor" id="btnSearchArtwork"></span>
							<input type="hidden" name="barcode_no" id="barcode_no" value="">
						</div>
						 <span class="text-danger"></span> 
						</div>
						<div class="offset-md-4 "></div>
					
				</div>					
					 <div class="row col-md-12 ">
					 <div class="col-md-5 offset-md-2 ">	
					<button type="submit" class="btn btn-primary" name="sbtAuditReport" id="sbtAuditReport">Search</button>
						<!--<a class="btn btn-dark mx-2" href="{{ route('dashboard') }}">Clear</a>-->
					<button type="button" class="btn btn-dark mx-2" name="btnCancel" id="btnCancel">Clear</button>						
				</div>	
					 </div>
									
		</div>		   	
		<div class="row">	
			<div class="block block-rounded">            
				<div class="block-content block-content-custom">
					<div class="col-lg-4  col-md-4 col-sm-12 col-xs-12 mb-2 pull-right">
						 <div class="input-group">
							<input class="form-control border" type="search" placeholder="Search" value="" id="searchbox">
								<span class="input-group-append">
									<button class="btn btn-outline-secondary bg-white border-bottom-0 border-0 rounded-pill ms-n5" type="button">
										<i class="fa fa-search"></i>
									</button>
								</span>								
						</div>					
					</div>						
				   <table class="table  table-bordered auditreportdata-table table-vcenter js-dataTable-full-pagination row-border" cellspacing="0">
					  <thead>
						<tr>
						  <th scope="col">id</th>
						  <th scope="col" class="dt-nowrap">Date</th>
						   <th scope="col">Action</th>
						   <th scope="col">QR Code</th>
						    <th scope="col">Bar Code</th>
							<th scope="col">Audit IP Address</th>	
						<th scope="col">User Agent</th>								
						  <!--<th scope="col">action</th>-->
						</tr>
						  </thead>
						  <tbody>
						  </tbody>
					</table>					
				</div>
			  </div>
			  <!-- END Inline --> 
		   
			
		</div>
		</div>
 <!-- END Page Content -->
@push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap-5.3.3/js/bootstrap.bundle.min.js') }}"></script>
    <!--
        OneUI JS

        Core libraries and functionality
        webpack is putting everything together at assets/_js/main/app.js
    -->
 <script src="{{ asset('/assets/admin/js/oneui.app.min.js') }}"></script>		
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('/assets/admin/js/jquery-ui.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() 
{	 
	if($(".searchBarCode").length>0)
	{
		$(".searchBarCode").on("keyup paste cut", function() 
		{
		var barcodeFetchURL =  "{{ url('admin/barCodes') }}";
		$(".searchBarCode").autocomplete({
			source: function(request, response) 
			{
				$.ajax({
					url: barcodeFetchURL,
					dataType: "json",
					//type: "POST",
					data: {
						searchText : request.term					
					},
					success: function(data) 
					{
						response(data);
						$(this).val('');
						//$('.artistLink').removeClass('hide');
					}
				});
			},
			select: function(event, ui) 
			{
				console.log(ui.item.id);				
				 $('#searchBarCode').val(ui.item.label); // display the selected text				
				if(ui.item.id)
				{							
				
				}
				else
				{
					return false;
				}

			},
			html: true,
			appendTo: '#search-content',			
			minLength:0,
			delay: 0,
		autofocus: true
		}).focus(function() 
		{
			if (this.value == "")
			{
				//$(this).autocomplete("search", "");
				console.log('focus event' + $(this).val() );
				$(this).autocomplete('close');
				$('#barcode_no').val("");
			}
			else{
				$(this).autocomplete('search',$(this).val());
			}
		}).keyup(function() 
		{
			if (this.value == "")
			{
				console.log('keyup event' + $(this).val() );
				//$(this).autocomplete("search", "");
				$(this).autocomplete('close');
				$('#barcode_no').val("");
			}
			else{
				$(this).autocomplete('search',$(this).val());
			}

		});	
		
	 });		
	
}
	loadAuditReport();

	 $('.searchBarCode').keypress(function(e) {		 
			 if($(this).val()!="")
			{
				$(this).autocomplete('close');
				var key = e.which;
				if (key == 13) // the enter key code
				{}
			}	
	 });
 
	$('#sbtAuditReport').on("click",function(e)
	{
			var error=0;				
			if($('#searchBarCode').val() ==""){
				$('.text-danger').html('The Bar code is required');
				error=1;
			}	
			else
			{
				$('.text-danger').html('');
			}
				
			if(error==0){
				loadAuditReport();
			}
	 });
	
	$('#btnCancel').on("click",function(e)
	{
		e.preventDefault();
		$('#searchBarCode').val("");
		$('#searchbox').val("");
		loadAuditReport();
	}); 
	 	 
	 
	$("#searchbox").on("keyup search input paste cut", function() {
	   $('.auditreportdata-table').DataTable().search(this.value).draw();
	});  
 });
 
 function loadAuditReport(){	
	   if($('#searchBarCode') && $('#searchBarCode').val()!="")
		{
			var searchBarCode = $('#searchBarCode').val();
		}
		else{
			var searchBarCode = "";
		}
	   var dataTable = $('.auditreportdata-table').DataTable({
      	processData: false,
		contentType: false,
		 responsive: true,
		   paging: true,
		 autoWidth:  false, 
		   destroy: true,
		    'sPaginationType':"full_numbers",
			"bInfo" : false,
		language: { search: '', searchPlaceholder: "Search...",
			//"sInfo" : "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong> "
			"sInfo" : "",
			},
		'oLanguage': {
		"sEmptyTable": "No data available",			
	  //"oPaginate": { "sPrevious": "", "sNext": ""}, 
	   sLengthMenu: "",
	  "oPaginate": {
		                "sFirst": "",
		                "sPrevious": "<span class='fa fa-angle-left'>",
		                "sNext": "<span class='fa fa-angle-right'>",
		                "sLast": "",
		            }
		},			
        //ajax: "{{ url('admin/auditreport') }}",
	    "ajax": {
					"url": "{{ url('admin/auditreport') }}",
					//"type": "POST",
					"data": function ( d ) {
						d.searchText= searchBarCode;                                        
					}
			},	      
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',  visible: false},
           {data: 'created_at', name: 'created_at', "orderable": false, searchable: true}, 	
			{data: 'action', name: 'action',"orderable": true, searchable: true},
			{data: 'qr_code', name: 'qr_code',"orderable": true, searchable: true}, 
			{data: 'bar_code', name: 'bar_code',"orderable": true, searchable: true}, 
			{data: 'ip_address', name: 'ip_address', "orderable": false, searchable: true},  
			{data: 'user_agent', name: 'user_agent', "orderable": false, searchable: true},             			
        ],
		"columnDefs": [{
		"targets": [-1],
		"className": "text-right"
		}],
		"aoColumnDefs": [{
		"aTargets": ['details'],
		"bSearchable": false,
		"bSortable": false
		}]
    });
}


</script>
@endpush
@stack('scripts')
@endsection