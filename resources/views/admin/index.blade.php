<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <meta name="description" content="Reeking Rich - Scratch card game">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Reeking Rich - Scratch game') }}</title>
	 <meta name="author" content="">
    <meta name="robots" content="">
    <!-- Open Graph Meta -->
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
	  <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="assets/media/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/media/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/media/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->  	
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
	<link href="{{ asset('/assets/bootstrap-5.3.3/css/bootstrap.min.css') }}" rel="stylesheet"> 
      <!-- Stylesheets -->
    <!-- OneUI framework -->
    <link rel="stylesheet" id="css-main" href="{{ asset('/assets/admin/css/oneui.min.css') }}">
	<link rel="stylesheet" id="css-main" href="{{ asset('/assets/admin/css/admin.css') }}">   
    <!-- END Stylesheets -->
</head>
<body>
    <div id="page-container">
      <!-- Main Container -->
      <main id="main-container">
        <!-- Page Content -->
        <div class="hero-static d-flex align-items-center">
          <div class="w-100">
            <!-- Sign In Section -->
            <div class="bg-body-extra-light">
              <div class="content content-full">
                <div class="row g-0 justify-content-center">
                  <div class="col-md-8 col-lg-6 col-xl-4 py-4 px-4 px-lg-5">
                    <!-- Header -->
                    <div class="text-center">
                      <p class="mb-5">
                        <img src="{{ asset('/assets/images/foundation_logo.svg') }}">
                      </p>
                      <h1 class="h4 mb-1">
                        Admin Login
                      </h1>
					  @if(Session::has('error-message'))
						<p class="alert alert-danger">{{ Session::get('error-message') }}</p>
					@endif			
				   @if (session('message'))
					<div class="alert alert-success" role="alert">
						{{ session('message') }}
					</div>
                    @endif				
                    </div>
                    <!-- END Header -->
                    <form class="frmSignin" action="{{ route('admin.checkAdminLoginStatus') }}" method="POST">
					 @csrf
                      <div class="py-3">
                        <div class="mb-4">
                          <input type="text" class="form-control form-control-lg form-control-alt" id="email" name="email" placeholder="Email Address" value="{{ old('email') }}">
						   @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="mb-4">
                          <input type="password" class="form-control form-control-lg form-control-alt" id="password" name="password" placeholder="Password" 
						  value="{{ old('password') }}">
						   @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>                      
                      </div>
                     <div class="row justify-content-center">
                        <div class="col-lg-6 col-xxl-5 d-flex justify-content-center align-items-center">
                          <button type="submit" class="btn btn btn-primary btn-login">
							Login
                          </button>
                        </div>
					</div>
					 <div class="row justify-content-center mt-1">
                        <div class="col-lg-6 col-xxl-5 d-flex justify-content-center align-items-center">                   
					    <a class="underline text-sm text-gray-600 hover:text-gray-900 fs-sm fw-medium" href="{{ route('forget.password.get') }}">
							{{ __('Forgot password?') }}
						</a>
					
                        </div>
					</div>					
                    </form>
                    <!-- END Sign In Form -->
                  </div>
                </div>
              </div>
            </div>
            <!-- END Sign In Section -->
            <!-- Footer -->
            <div class="fs-sm text-center text-muted py-3">
              <a class="fw-semibold" href="#">Copyright</a> &copy; <span data-toggle="year-copy"></span>
			  Rieves All rights reserved
            </div>
            <!-- END Footer -->
          </div>
        </div>
        <!-- END Page Content -->
      </main>
      <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</body>
</html>
@push('scripts')
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap-5.3.3/js/bootstrap.bundle.min.js') }}"></script>
 <script src="{{ asset('/assets/admin/js/oneui.app.min.js') }}"></script>
 <script src="{{ asset('/assets/admin/js/lib/jquery.min.js') }}"></script>  
@endpush