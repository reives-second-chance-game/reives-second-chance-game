@extends('admin.layouts.app_admin')
@section('content')
<style type="text/css">
		.loader-div {
			display:none;
			position: fixed;
			margin: 0px;
			padding: 0px;
			right: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			background-color: #fff;
			z-index: 30001;
			opacity: 0.8;
		}
		.loader-img {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			margin: auto;
			height: 350px;
			width: auto;
		}
	</style>
	     <!-- Hero -->       
            <div class="content ">
              <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-start">
                <div class="flex-grow-1">
                  <h2 class="fw-semibold text-dark mb-0">Upload CSV</h2>           
                </div>
                <div class="flex-shrink-0 mt-3 mt-sm-0 ms-sm-3">
                  <span class="d-inline-block">
                   
                  </span>
                </div>
              </div>
            </div>         
        <!-- END Hero -->
       
        <!-- Page Content -->
        <div class="content">
			<div class="row">
				<div class="col-md-12">				
					<!--<div class="alert d-flex align-items-center d-none">                      
					</div>--> 
         			 @if(Session::has('errorsCSV') )
                    <div class="alert alert-danger text-break">
                       {!! session('errorsCSV') !!}
                    </div>
						@endif				
						
				  <!-- Inline -->
				  @if(Session::has('error'))
				  <div class="alert alert-danger text-break">					
					{!! session('error') !!}
				  </div>
				  @endif

				  @if(Session::has('message'))
				  <div class="alert alert-success">
					{{ Session::get('message')}}
				  </div>
				  @endif	
				   <div class="loader-div">
						<img class="loader-img" src="{{ asset('/assets/images/loader-ajax.gif') }}" />
					</div>			
					<!--<div class="content bg-white push progressdiv d-none ">             
						<div class="progress active" id="imageProgress">
							<div id="myBar" style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-info progress-bar-striped"></div>
						</div>                  
					</div> -->          
				  <div class="block block-rounded">            
					<div class="block-content block-content-full">	
					  <!--<form action="{{ route('uploadCSV') }}" method="POST" enctype="multipart/form-data">-->			  
					   <form action="{{ route('uploadCSV') }}" method="POST" enctype="multipart/form-data" onsubmit="return fnValidate();">
							@csrf
					  <div class="mb-4 row">				
						<label for="inputPassword" class="col-sm-2 col-form-label">Browse CSV</label>
							<div class="col-sm-5">
							  <input class="form-control csvBrowse" type="file" id="formFile" name="file"  accept=".csv">
							  <span class="text-danger" role="alert">								
								</span>
							</div>						 				
						</div>
						<div class="form-group row">
							<div class="col-sm-5">
								<button type="submit" id="submitCSV" class="btn btn-primary">Submit</button>
								<a class="btn btn-dark mx-2" href="{{ route('dashboard') }}">Cancel</a>						
							</div>				
						</div>
					</form>
					</div>
				  </div>
				  <!-- END Inline -->       
				</div>
			</div>
		</div>
        <!-- END Page Content -->
		
@push('scripts')	
<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('/assets/bootstrap-5.3.3/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('/assets/admin/js/oneui.app.min.js') }}"></script>	
<script type="text/javascript">
$(document).ready(function() 
{	
	/*$('#frmcsvUpload').on('submit', function(e)
	{
		e.preventDefault();
		$(".loader-div").show(); // show loader		
		
		$.ajax({
		 url: '{!! route('uploadCSV') !!}',
		 type:"POST",
			async:false,		 
			contentType: false,
			cache: false,
			processData:false,
		data:  new FormData(this),	
		beforeSend : function(){				

		},		
		 success:function(response){
		 
			 if(response.status=="success")
			 {
			   $('.alert').removeClass('d-none');
			   $('.alert').addClass('alert-success');
			   $('.alert'). html(response.message);
			   $('#formFile').val("");
			  $(".loader-div").hide(); // hide loader
			 }
			 else
			 {
				$(".loader-div").hide(); // hide loader
				$('.alert').removeClass('d-none');
			   $('.alert').addClass('alert-danger');	
			   $('.alert'). html(response.message);
			    $('#formFile').val("");
			 }
		  },
		  error: function (xhr, status) {
			$(".loader-div").hide(); // hide loader	
			
		  }
		});
		
	});*/
   
   
   
});

function fnValidate()
{
		var error=0;				
	if($('#formFile').val() ==""){
		$('.text-danger').html('The CSV file is required.');
		error=1;
		return false;
	}		
	else
	{
		$('.text-danger').html('');
	}			
	if(error==0){
		$(".loader-div").show(); // show loader		
		//$('#formFile').val("");
		return true;
	}
}

</script>
@endpush
@stack('scripts')
    @endsection