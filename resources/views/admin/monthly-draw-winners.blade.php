@extends('admin.layouts.app_admin')
@section('content')
 <!-- Page Label -->  
<!--<script src="{{ asset('/assets/js/jquery-2.2.4.min.js') }}"></script>-->
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="{{ asset('/assets/bootstrap-5.3.3/js/bootstrap.bundle.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('/assets/admin/css/jquery-ui.css') }}">
    <!--
        OneUI JS
        Core libraries and functionality
        webpack is putting everything together at assets/_js/main/app.js
    -->
 <script src="{{ asset('/assets/admin/js/oneui.app.min.js') }}"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
 <!--<script type="text/javascript" src="{{ asset('/assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>-->
  <!--<script type="text/javascript" src="{{ asset('/assets/admin/js/jquery.dataTables.min.js') }}"></script>-->
 <script src="{{ asset('/assets/admin/js/jquery-ui.js') }}"></script>

    <!--
        OneUI JS

        Core libraries and functionality
        webpack is putting everything together at assets/_js/main/app.js
    -->
    
	<div class="content ">
	  <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-start">
			<div class="flex-grow-1">
				<h2 class="fw-semibold text-dark mb-0">Monthly draw Winners</h2>           
			</div>
			<div class="flex-shrink-0 mt-3 mt-sm-0 ms-sm-3">
				<span class="d-inline-block">                   
				</span>
			</div>
	  </div>
	</div>         
        <!-- END Page Label -->       
        <!-- Page Content -->
        <div class="content exportdataSection">	
			<div class="row">		
				<div class="alert d-none">					
				</div>
			
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d-flex align-items-center ">
				         <!-- Form Horizontal - Default Style -->
					<!-- <form class="frmExportData" id="frmExportData" action="{{ url('/admin/exportQRList') }}" method="GET">-->
					<form class="frmDraw" id="frmDraw">
					@csrf
						<div class="row mb-3">
							<label class="col-sm-4 col-form-label" for="example-hf-password">Game No:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control"  id="gameNo" name="gameNo"  maxlength="3"> 
								<span class="gameNoErr text-danger"></span>   
						  </div>
						</div>
						<div class="row mb-3">
						  <label class="col-sm-4 col-form-label" for="example-hf-email">QR Code:</label>
						  <div class="col-sm-8">
							<input type="text" class="form-control" id="qrcode" name="qrcode" maxlength="9"> 
							<span class="qrCodeErr text-danger"></span>   
						  </div>
						</div>                	
						<div class="row mb-3">
							<label class="col-sm-4 col-form-label" for="example-hf-password">Draw Date:</label>
							<div class="col-sm-8">
								<input type="text" class="form-control datePicker"  id="drawDate" name="drawDate" readonly> 
								<span class="drawDateErr text-danger"></span> 
							</div>
						</div>
						<div class="row">
							<div class="col-sm-8 ms-auto">
								<button type="submit" class="btn btn-primary" id="btnSubmitExport">Submit</button>  
								<button type="button" class="btn btn-dark mx-2" name="btnCancel" id="btnCancel">Clear</button>
							</div>
						</div>
					 </form>
				</div>			
			</div>	
		<div class="row">	
			<div class="block block-rounded">            
				<div class="block-content block-content-custom">
					<div class="col-lg-4  col-md-4 col-sm-12 col-xs-12 mb-2 pull-right">
						 <div class="input-group">
							<input class="form-control border" type="search" placeholder="Search QR Code " value="" id="searchbox">
								<span class="input-group-append">
									<button class="btn btn-outline-secondary bg-white border-bottom-0 border-0 rounded-pill my-1 ms-n5" type="button">
										<i class="fa fa-search"></i>
									</button>
								</span>								
						</div>					
					</div>						
				   <table class="table  table-bordered monthlydrawWinners-table table-vcenter js-dataTable-full-pagination row-border" cellspacing="0">
					  <thead>
						<tr>
						  <th scope="col">id</th>
						  <th scope="col" class="dt-nowrap">Draw Date</th>
						   <th scope="col">Game No</th>	
						   <th scope="col">QR Code</th>				   					
							 <th scope="col">Action</th>	
						</tr>
						  </thead>
						  <tbody>
						  </tbody>
					</table>					
				</div>
			  </div>
			  <!-- END Inline --> 		   
			
		</div>			
		</div>
        <!-- END Page Content -->
		
		<!-- Fade In Block Modal -->
          <div class="modal fade confirmModal" id="modal-block-fadein" tabindex="-1" role="dialog" aria-labelledby="modal-block-fadein" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="block block-rounded block-transparent mb-0">
                  <div class="block-header block-header-default">
                    <h3 class="block-title">Confirm</h3>
                    <div class="block-options">
                      <button type="button" class="btn-block-option" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-fw fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <div class="block-content fs-sm">
                    <p>This QR Code is already selected for Monthly Draw. Do you want to continue?</p>
                  </div>
                  <div class="block-content block-content-full text-end bg-body">
                    <button type="button" class="btn btn-sm btn-alt-secondary me-1" data-bs-dismiss="modal">No</button>
                    <button type="button" class="btn btn-sm btn-primary storeMonthlyDrawConfirm" data-bs-dismiss="modal">Yes</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- END Fade In Block Modal -->
		  
		    <!-- Slide Left Block Modal -->
          <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete" aria-hidden="true">
            <div class="modal-dialog modal-dialog-slideleft" role="document">
              <div class="modal-content">
                <div class="block block-rounded block-transparent mb-0">
                  <div class="block-header block-header-default">
                    <h3 class="block-title">Confirm Delete</h3>
                    <div class="block-options">
                      <button type="button" class="btn-block-option" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-fw fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <div class="block-content fs-sm">
                    <p>Do you want to delete this record?</p>
                  </div>
                  <div class="block-content block-content-full text-end bg-body">
                    <button type="button" class="btn btn-sm btn-alt-secondary me-1" data-bs-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-sm btn-primary btnDeleteRecord" data-id="" data-bs-dismiss="modal">Delete</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- END Slide Left Block Modal -->
		  
		  
		
		
	@push('scripts')
<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
</script>
<script type="text/javascript">
$(document).ready(function() 
{
  	  var dataTable = $('.monthlydrawWinners-table').DataTable({
    	processData: false,
		contentType: false,
		 processing: true,
          serverSide: true,         
           paging: true,
		 "responsive": true,
			autoWidth:  false,
		  destroy: true,
		  'iDisplayLength': 10,
		  	"bInfo" : false,
		    //scrollY:        "100vh",
			scrollX:        true,
	    scrollCollapse: true,		
		language: { search: '', searchPlaceholder: "Search...",
			//"sInfo" : "Page <strong>_PAGE_</strong> of <strong>_PAGES_</strong> "
			"sInfo" : "",
			},
		'oLanguage': {
		"sEmptyTable": "No data available",			
	  //"oPaginate": { "sPrevious": "", "sNext": ""}, 
	   sLengthMenu: "",
	  "oPaginate": {
		                "sFirst": "",
		                "sPrevious": "<span class='fa fa-angle-left'>",
		                "sNext": "<span class='fa fa-angle-right'>",
		                "sLast": "",
		            }
		},			
        //ajax: "{{ url('admin/auditreport') }}",
	    "ajax": {
					"url": "{{ url('admin/monthly-draw-winners') }}",
					//"type": "POST",
					"data": function ( d ) {
							   if($('#searchbox') && $('#searchbox').val()!="")
							{
								var searchbox = $('#searchbox').val();
							}
							else{
								var searchbox = "";
							}							
						
						d.searchText= searchbox;                                        
					}
			},	      
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',  visible: false},
			{data: 'draw_date', name: 'draw_date', "orderable": false, searchable: true}, 	
			{data: 'game_no', name: 'game_no',"orderable": true, searchable: true}, 			
			{data: 'qr_code', name: 'qr_code',"orderable": true, searchable: true}, 
			{data: 'action', name: 'action',"orderable": true, searchable: false},
			
        ],
		"columnDefs": [{
		"targets": [-1],
		"className": "text-right"
		}],
		"aoColumnDefs": [{
			"aTargets": ['details'],
			"bSearchable": false,
			"bSortable": false
		}]
    });
	
	$("#searchbox").on("keyup search input paste cut", function() {	
		//dataTable.search(this.value).draw();
		  dataTable.draw();		
}); 
	
	
	$('.datePicker').datepicker({
		  autoclose: true,      
			 autoclose: true,
		  todayHighlight: true,
		  format: "dd/mm/yyyy",
		   altFormat: "dd/mm/yy", 
            dateFormat: 'dd/mm/yy',
			minDate: 0,
		 //maxDate: 0,
		  beforeShow: function(){ 
			$(".ui-datepicker").css('font-size', '95%');
			$(".ui-datepicker").css('width', '17em');
		}	
	});
	
	$('#btnSubmitExport').on("click",function(e)
	{
		e.preventDefault();
		var error=0;	
		$('.alert').addClass('d-none');
		$('.alert').removeClass('alert-success');
		$('.alert').removeClass('alert-danger');
		$('.alert').html("");	
				
		if($('#gameNo').val() ==""){
			$('.gameNoErr').html('The Game No is required.');
			error=1;
		}	
		else
		{
			$('.gameNoErr').html('');
		}
		if($('#qrcode').val() ==""){					
				$('.qrCodeErr').html('The QR Code is required.');
				error=1;
		}	
		else
		{
			$('.qrCodeErr').html('');
		}	
		if($('#drawDate').val()=="")	
		{
				$('.drawDateErr').html('The Draw Date is required');					
				error=1;					
		}	
		else
		{
			$('.drawDateErr').html('');					
		}		
		
		if(error==0){
				 // Serialize the form data
           var form = $('#frmDraw').get(0); 
			var formData = new FormData(form);
            // Send an AJAX request
           $.ajax({
                type: 'POST',
                url: '{!! route('monthlydrawWinners.store') !!}',
				processData: false,
				contentType: false,
                data: formData,
                //dataType: 'json',
                success: function(response) {
					//alert(response.status);
					if(response.status=="success")
					{
						$('.alert').removeClass('d-none');
						$('.alert').removeClass('alert-danger');
						$('.alert').addClass('alert-success');
						$('.alert').html(response.message);		
						dataTable.draw();
						$("#frmDraw")[0].reset();
					}
					else
					{						
						if(response.status=="toConfirm")
						{
							$('.confirmModal').modal('show');
						}
						else
						{
							$('.alert').removeClass('d-none');
							$('.alert').removeClass('alert-success');
							$('.alert').addClass('alert-danger');
							$('.alert').html(response.message);		
						}	
					}
					
                    // Handle the response message
                   // $('#cf-response-message').text(response.message);
                },
                error: function(xhr, status, error) {                   
                }
            });
		}
	 });
	 
	 
	$('.storeMonthlyDrawConfirm').on("click",function(e)
	{
		var form = $('#frmDraw').get(0); 
		var formData = new FormData(form);
		formData.append('action','modalConfirmation');
		$.ajax({
                type: 'POST',
                url: '{!! route('monthlydrawWinnersConfirm.store') !!}',
				processData: false,
				contentType: false,
                data: formData,
                //dataType: 'json',
                success: function(response) {
					//alert(response.status);
					if(response.status=="success")
					{
						$('.alert').removeClass('d-none');
						$('.alert').removeClass('alert-danger');
						$('.alert').addClass('alert-success');
						$('.alert').html(response.message);		
						dataTable.draw();
						$("#frmDraw")[0].reset();
						$('.confirmModal').modal('hide');
					}
					else
					{						
						$('.alert').removeClass('d-none');
						$('.alert').removeClass('alert-success');
						$('.alert').addClass('alert-danger');
						$('.alert').html(response.message);		
							
					}
					
                    // Handle the response message
                   // $('#cf-response-message').text(response.message);
                },
                error: function(xhr, status, error) {                   
                }
            });
		
	 });	
	 
	 let $modalDelete = $('#modal-delete');
	 
	 $('.monthlydrawWinners-table').on('click', '.del_' ,function(e){
		e.preventDefault();
		$('.alert').addClass('d-none');
		$('.alert').removeClass('alert-success');
		$('.alert').removeClass('alert-danger');
		$('.alert').html("");
		
		 var id = $(this).data('id');  
		//alert('id ' + id + ' was clicked');    
		$('.btnDeleteRecord').attr('data-id', id);
		$modalDelete.modal('show');
	});
	
	$('.btnDeleteRecord').on('click', function(e)
	{
		var token = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			type: 'POST',
			url: '{!! route('monthlydrawWinners.delete') !!}',
			
			//data: $(this).attr('data-id'),			
			 data: {
			id: $(this).attr('data-id'),
			_token: token
			},
			//dataType: 'json',
			success: function(response) {
			//alert(response.status);
			if(response.status=="success")
			{
				$('.alert').removeClass('d-none');
				$('.alert').addClass('alert-success');
				$('.alert').removeClass('alert-danger');
				$('.alert').html(response.message);
				dataTable.draw();
				setTimeout(function() 
				{ 				
					$('.alert').addClass('d-none');
					$('.alert').removeClass('alert-success');
					$('.alert').removeClass('alert-danger');
					$('.alert').html("");				
				}, 3000);					
				
				
			}
			else
			{						
					
			}                 
		},
		error: function(xhr, status, error) {                   
		}
		});
	
	});
	
	$('#btnCancel').on("click",function(e)
	{
		e.preventDefault();
		$('#gameNo').val("");
		$('#qrcode').val("");
		$('#drawDate').val("");
		$('.gameNoErr ').html('');
		$('.qrCodeErr ').html('');		 
		$('.drawDateErr ').html('');
		$('#searchbox').val("");
		$('.alert').html("");
		$('.alert').removeClass("alert-danger");
		$('.alert').removeClass("alert-success");
		$('.alert').addClass('d-none');
		 dataTable.draw();			
	});	 
	$('.dataTables_filter input').addClass('form-control');
  
  });
  
  $(document).on( "change", $('.datepicker'), function() {	 
$(this).datepicker('hide');
});





</script>
@endpush
@stack('scripts')
    @endsection