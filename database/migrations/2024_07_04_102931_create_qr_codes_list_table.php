<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('qr_codes_list', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('game_no');
            $table->string('retailer', 200);
            $table->longText('qr_code'); 
            $table->longText('bar_code'); 
            $table->tinyInteger('win_type'); 
            $table->decimal("price", 7, 2);
            $table->date("expiry_date");
            $table->longText("url");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('qr_codes_list');
    }
};
