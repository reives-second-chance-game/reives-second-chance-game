<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('scanned_qrcodes', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('fk_qr_codes_list');
            $table->foreign('fk_qr_codes_list')->references('id')->on('qr_codes_list');
           //$table->foreignId('fk_qrcodes_list')->constrained();
            $table->text('scanned_url'); 
            $table->string('scanned_type', 100);
			$table->tinyInteger('game_attempts');
			$table->tinyInteger('card_attempts')->comment('No.of cards scratched');
			$table->tinyInteger('game_status')->comment('0-pending, 1-completed');  
			$table->text('token_id');  			 
            $table->tinyInteger('win_type')->comment('1-Online winner, 0-looser, 2-Ticket winner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('scanned_qrcodes');
    }
};
