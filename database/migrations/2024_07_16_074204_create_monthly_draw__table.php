<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('monthly_draw', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('fk_qr_codes_list');
            $table->foreign('fk_qr_codes_list')->references('id')->on('qr_codes_list');          
            $table->string('email');         
			$table->string('ip_address');			
            $table->longText('user_agent'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('monthly_draw');
    }
};
