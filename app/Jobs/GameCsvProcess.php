<?php

namespace App\Jobs;
use Throwable;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\Models\qrcodeslist;
use Carbon\Carbon;


class GameCsvProcess implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $data;
    public $header;
    /**
     * Create a new job instance.
     */
    public function __construct($data, $header)
    {
        $this->data   = $data;
        $this->header = $header;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $errorsCSV = array();
		set_time_limit(0);
		
		foreach ($this->data as $sale) 
        {
           $saleData = array_combine($this->header, $sale); 
			$QRCodeLength = "";
			$gameNo = "";
			$errCnt = 0;
            if((isset($saleData['QRCode'])  && $saleData['QRCode']!="" 
				&&  isset($saleData['BarCode']) &&  $saleData['BarCode']!="" 
				&&  isset($saleData['Wintype']) && $saleData['Wintype']!=""  
				&&  isset($saleData['Prize Amount']) &&  $saleData['Prize Amount']!="" 
				&& isset($saleData['EndDate']) && $saleData['EndDate']!="" 
				&& isset($saleData['URL']) && $saleData['URL']!=""))
				{                
				   if(!preg_match('/(0[1-9]|1[0-9]|2[0-9]|3(0|1))\/(0[1-9]|1[0-2])\/\d{4}/', $saleData['EndDate']))
					{                       
						$enddate = "";  
						$errCnt = 1;						
						
					} 
					else
					{           
						$enddate = Carbon::createFromFormat('d/m/Y', $saleData['EndDate'])->format('Y-m-d');
					}
					$QRCodeLength = Str::length($saleData['QRCode']);
					if($QRCodeLength!="" && $QRCodeLength==9)
					{                
						$gameNo = substr($saleData['QRCode'], 0, 3);
					} 
					else
					{           
						$gameNo = ""; 
						$errCnt = 1;
					} 					
					
					if($errCnt==0)
					{
						$count=qrcodeslist::where('qr_code', $saleData['QRCode'])->count();
						if($count>0){}
						else
						{
							qrcodeslist::create(               
									[
										'qr_code' => $saleData['QRCode'],
										'game_no' => $gameNo,
										'bar_code' => $saleData['BarCode'],
										'win_type' => $saleData['Wintype'],
										'prize_amount' =>  $saleData['Prize Amount'],
										'expiry_date' =>  $enddate,
										'url' => $saleData['URL']
									]
								);
							}
					}else{
						  array_push($errorsCSV, $saleData['QRCode']); 
					}					
           }
           else{ 
				if($saleData['QRCode']==""){} else{
					array_push($errorsCSV, $saleData['QRCode']);   
				}
           }     
        } 
		if(count($errorsCSV)>0){
			Session::push('csvError',$errorsCSV);
		}else{
			
		}
    }

    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc...
    }
}
