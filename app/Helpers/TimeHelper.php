<?php

// app/Helpers/TimeHelper.php

use Carbon\Carbon;
if (!function_exists('convertToLocale')) {
function convertToLocale($time)
{
    return Carbon::parse($time)->timezone(config('app.timezone'));
}
}

