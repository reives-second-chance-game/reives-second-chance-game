<?php

namespace App\Http\Controllers;
use App\Jobs\GameCsvProcess;
use Illuminate\Support\Facades\Bus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\qrcodeslist;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Validator;
class csvController extends Controller
{
    //
    public function uploadCSV(Request $request)
    {
        $file = $request->file('file');    
	  
		$rules = [
			'file' => 'required|file|mimetypes:text/csv,application/csv,text/comma-separated-values|max:51200'		
		];
		$messages = [
			'file.required' => 'The CSV file is required.',
			'file.mimetypes' => 'Upload only CSV file.',
			'file.max' => 'Max file Size should not be more than 50MB.'
		];
		
		//$request->validate($rules, $messages);
		
		$errorMsg =  "";
		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->fails())
		{
			$errorMsg = "<ul class='mb-0'>";
			foreach ($validator->messages()->toArray() as $key => $value) {					
				$errorMsg .= "<li>".$value[0].'</li>';
			}
			$errorMsg .= "</ul>";
			//return response(['message' => $errorMsg, 'status' => 'fail']);
			session()->flash('error', $errorMsg); 
            return redirect()->to('/admin/qrcode-list'); 			
		}
			
		$colHeader = [
            0 =>'QRCode', 1=>'BarCode', 2=>'Wintype', 3=>'Prize Amount', 4=>'EndDate', 5=>'URL' 
        ];
				
		//$file = file(request()->file); //$request->file('file');	
		$uname = uniqid();
        $oname = $file->getClientOriginalName();
        $filename = $uname.$oname;	
        $filelocation = public_path('assets/csv');
		$filepath = $file->move($filelocation, $filename);
		// Create an empty Batch and then dispatch it
        $batch = Bus::batch([])->dispatch();	
		//$chunks = array_chunk($file, 1000);
		$chunks = array_chunk(file(public_path('assets/csv/' . $filename)), 1000);
		$header = [];		
		set_time_limit(0);
		$dataIns=0;
		$errors = array();
		$dataCnt=0;	
			session()->forget('csvError');				
		foreach($chunks as $key => $chunk) {
			// Get File Content and save it as an array
			$errorCnt = 0;							
			$data = array_map('str_getcsv', $chunk);
			
			if ($key === 0) {
				$header = $data[0];					
				 $headerArr = array_change_key_case($header, CASE_LOWER);				 
				
				   if(!isset($headerArr[0]) || $headerArr[0]!=$colHeader[0]) {                   
                   // return response(['message' => 'QRCode column is missing in the CSV file', 'status' => 'fail']);
				   session()->flash('error', 'QRCode column is missing in the CSV file'); 
                    return redirect()->to('/admin/qrcode-list'); 
                }
				else if(!isset($headerArr[1]) || $headerArr[1]!=$colHeader[1]) {                   
                   //return response(['message' => 'BarCode column is missing in the CSV file', 'status' => 'fail']);
				    session()->flash('error', 'BarCode column is missing in the CSV file'); 
                    return redirect()->to('/admin/qrcode-list'); 
                } 
               else if(!isset($headerArr[2]) || $headerArr[2]!=$colHeader[2]) { 		
                    session()->flash('error', 'Wintype column is missing in the CSV file'); 
                    return redirect()->to('/admin/qrcode-list'); 
                }
                else if(!isset($headerArr[3]) || $headerArr[3]!=$colHeader[3]) {                    
                    // return response(['message' =>  'Prize amount column is missing in the CSV file', 'status' => 'fail']);
					 session()->flash('error', 'Prize amount column is missing in the CSV file'); 
                    return redirect()->to('/admin/qrcode-list'); 
                } 
               else if(!isset($headerArr[4]) || $headerArr[4]!=$colHeader[4]) {                     
                    //return response(['message' =>  'EndDate column is missing in the CSV file', 'status' => 'fail']);
					 session()->flash('error', 'EndDate column is missing in the CSV file'); 
                    return redirect()->to('/admin/qrcode-list'); 
                } 
				 else if(!isset($headerArr[5]) || $headerArr[5]!=$colHeader[5]) {                     
                   //return response(['message' =>  'URL column is missing in the CSV file', 'status' => 'fail']);
				   session()->flash('error', 'URL column is missing in the CSV file'); 
                    return redirect()->to('/admin/qrcode-list'); 
                }
                else{} 				
				unset($data[0]);				
			}
		
			$batch->add(new GameCsvProcess($data, $header));	
						
			}		
			@unlink($filelocation.'/'.$filename);
			Storage::delete($file);				 	
			if(Session::has('csvError'))
			{
				$errors = Session::get('csvError');				
				$errors = array_map("unserialize", array_unique(array_map("serialize", $errors)));
				$errorsMerge = call_user_func_array("array_merge", $errors);
			}else{}						
		//return response(['message' => $batch->id, 'status' => 'success']);
		 // Redirect And Return the batch to get the information about the process
		//return redirect(url('admin/batch/'.$batch->id));			
		if(count($errors)>0){
            session()->flash('errorsCSV', "These QRCodes not added successfully ".implode(",",array_unique($errorsMerge))); 
                    return redirect()->to('/admin/qrcode-list');   
       }		
     else if($batch->id!=""){
            session()->flash('message', 'CSV file is imported successfully.'); 
                    return redirect()->to('/admin/qrcode-list');   
        }		
		else{
            session()->flash('error', 'The CSV file data was not added successfully'); 
                  return redirect()->to('/admin/qrcode-list');   
        }
			
	}

	public function batch() {
			$batchId = request()->id;
			$batch = Bus::findBatch($batchId);
			return view('admin/batch-progress', compact('batch'));
	}    

}
