<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request; 
use DB; 
use Carbon\Carbon; 
use App\Models\Admin; 
use Mail; 
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use validator;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    //use SendsPasswordResetEmails;
	
	public function showForgetPasswordForm()
	  {
		 return view('admin.auth.passwords.email');
	  }
	  
	public function submitForgetPasswordForm(Request $request)
    {
		 //$input = $request->only('email');
        
		        
		$rules = [
			'email' => 'required|email|exists:admins'		
		];

		$messages = [
			'email.required' => 'The email is required.',
			'email.email' => 'The email is not valid format.',
			'email.exists' => 'This email is not registered in the system.',
		];	
		
		
       $request->validate($rules, $messages);

       /*$request->validate([
              'email' => 'required|email|exists:admins',
        ]);*/
		  
		 $token = Str::random(64);
  
          DB::table('password_resets')->insert([
              'email' => $request->email, 
              'token' => $token, 
              'created_at' => Carbon::now()
            ]);
			
			 $message = "Dear User,"."\n\n";
            $message .= "You have requested to reset your password."."\n";
            $message .= "To reset your password, click the following link and follow the instructions."."\n";          
            $message .= env('BASE_URL')."admin/reset-password?token=".$token."\n\n";
            $message .= "Thanks"."\n";
            $message .= "Morrisons Foundation";
			 Mail::send('admin.Email.forgetPassword', ['token' => $token], function($message) use($request){
                  $message->to($request->email);
                  $message->subject('Reset Password');
                });
			return back()->with('message', 'We have e-mailed your password reset link!');
	}	
	
	public function showResetPasswordForm(Request $request) {		
		if($request->token){
			parse_str($request->token, $params);		
			return view('admin.auth.passwords.reset', ['token' => $params['token']]);		
		} 
    }
	
	
	public function submitResetPasswordForm(Request $request)
    {
        /*$request->validate([
              'email' => 'required|max:200|email|exists:admins',
              //'password' => 'required|min:6|confirmed',
			   'password' => [
				'required',
				'string',
				Password::min(6)
					->mixedCase()
					->numbers()
					->symbols()
					//->uncompromised()
				//'confirmed'
				],
              'password_confirmation' => 'required'
          ]);*/
		  
		  $rules = [
			'email' => 'required|max:200|email|exists:admins',
			'password' => 'required|string|min:6|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
			 'password_confirmation' => 'required'
		];

		$messages = [
			'email.required' => 'The email is required.',
			'email.email' => 'The email is not valid format.',
			'email.exists' => 'This email is not registered in the system.',
			'password.confirmed' => 'Confirm Password doesn\'t match',
			'password.regex' => 'Password must contain at least one number and both uppercase and lowercase letters and one special character.'
		];	
		
		
       $request->validate($rules, $messages);
		  
	//\DB::enableQueryLog(); // Enable query log
          $updatePassword = \DB::table('password_resets')
                              ->where([
                                'email' => $request->email, 
                                'token' => $request->token
                              ])
                              ->first();
							  
	//dd(\DB::getQueryLog()); // Show results of log
          if(!$updatePassword){			  
              return back()->withInput()->with('error', 'Invalid token!');
          } 
          $user = \DB::table('admins')->where('email', $request->email)
                      ->update(['password' => Hash::make($request->password)]);
 
          \DB::table('password_resets')->where(['email'=> $request->email])->delete();
  
          return redirect('admin/login')->with('message', 'Your password has been changed!');
    }
	  
	  
	 public function testEmail()
    {
        $to_name = "Anitha";
        $to_email = "anikadrib@gmail.com";
        $data = array("name"=>"Vitalis", "body" => "A test mail");
        Mail::send('Email.mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
            ->subject('Laravel Test Mail');
            $message->from('admin@morrisons.com','Admin');
        });
    }

	
	
}
