<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{qrcodeslist, ScannedQrCodes, gameOutcomes, monthlyDraw, DataLogging, monthlyDrawWinners};
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\File;
use Hibit\GeoDetect;

class gameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('ScannedQrCodes')->except(['index', 'qrCodeManualCheck', 'play']);
    }
	
	
	//
    public function index(Request $request) 
	{	
		//Refer this: http://127.0.0.1:8000/280639782	
		if(isset($request->qrcode) && $request->qrcode!="")
		{
			/*if(isset(Auth::guard('ScannedQrCodes')->user()->qr_code) && Auth::guard('ScannedQrCodes')->user()->qr_code!=$request->qrcode){
				$error = ['message'=>'This QR code is not allowed to access in the system', 'status'=>'fail'];									
				return view('frontend.play', compact('error'));
			}
			else
			{*/				
				$todayDate = Carbon::now()->format('Y-m-d');			 
				$QRCode =  isset($request->qrcode) ?$request->qrcode : "";
				//$resultQrCode = qrcodeslist::where('qr_code',''.$QRCode.'')->where('expiry_date', '>=', $todayDate)->get()->toArray();
				$result = qrcodeslist::where('qr_code',''.$QRCode.'')->get()->first();
				if($result)
					$resultQrCode = $result->toArray();
				else
					$resultQrCode = array();
				if(count($resultQrCode)>0) 
				{			
					if($resultQrCode['expiry_date']<=$todayDate){
						$error = ['message'=>'This QR Code is expired', 'status'=>'fail'];
							return view('frontend.play', compact('error'));
					}
					else if($resultQrCode['qrcode_status']==1){
						$error = ['message'=>'This code has already been redeemed  <br>For further assistance, please contact us at  <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>', 'status'=>'fail'];	
							return view('frontend.play', compact('error'));
					}
					else{
						 if(Session::has('scanned_type'))
						 {
							  session()->forget('scanned_type');
							  Session::put('scanned_type', 'URL');
						 }
						 else{
							  Session::put('scanned_type', 'URL'); 
						 }						
						Auth::guard('ScannedQrCodes')->login($result);
						$user = auth()->guard('ScannedQrCodes')->user();
						$userAgent = $request->userAgent();
						$ip = $_SERVER['REMOTE_ADDR'];	
						$todaysDateTime = Carbon::now()->format('Y-m-d H:i:s');
						$scannedType = Session::get('scanned_type');
						$dataLog= array();
						$dataLog = [
							'fk_qr_codes_list' =>  $user->id,  
							 'action' =>"Scanned QR code",
							 'scanned_type' => $scannedType,
							'page_name' =>  "Visited Game Play page",			
							'ip_address' =>$ip,
							'user_agent' => $userAgent,
							'created_at'=> $todaysDateTime				
						];
						$id = DataLogging::create($dataLog)->id;						
						 session()->flash('status', 'success');					
						return redirect()->to('play'); 
					}				
				}
				else
				{ 
					$error = ['message'=>'Invalid QR code. <br>For further assistance, please contact us at  <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a> ', 'status'=>'fail'];									
					return view('frontend.play', compact('error'));
				}
			//}	
		}
		else{
			return view('frontend.play-manual');
		}
		
	}
	
	public function play()
	{
		if(Auth::guard('ScannedQrCodes')->check())
		{
			return view('frontend.play');
		}
		else
		{
			return redirect()->to('not-authorized'); 
		}		
	}
	
	public function qrCodeManualCheck(Request $request) 
	{
		 $QRCode =  trim($request->qrcode);
		  $todaysDate = Carbon::now()->format('Y-m-d');
		  $todaysDateTime = Carbon::now()->format('Y-m-d H:i:s');
			$result = qrcodeslist::where('qr_code',''.$QRCode.'')->get()->first();
			if($result)
				$resultQrCode = $result->toArray();
			else
				$resultQrCode = array();
		if(count($resultQrCode)>0) 
		{			
			if($resultQrCode['expiry_date']<=$todaysDate){
					$message = 'This QR Code is expired. For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour errorMsg">admin@rieves.co.uk</a>';
					return response(['message' => $message, 'status' => 'fail']);
			}
			else if($resultQrCode['qrcode_status']==1){
				$message = 'This code has already been redeemed. For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour errorMsg">admin@rieves.co.uk</a>';	
				return response(['message' => $message, 'status' => 'fail']);
			}
			else{
				Auth::guard('ScannedQrCodes')->login($result);
				$user = auth()->guard('ScannedQrCodes')->user();
				 if(Session::has('scanned_type'))
				 {
					  session()->forget('scanned_type');
					  Session::put('scanned_type', 'Manual');
				 }
				 else{
					  Session::put('scanned_type', 'Manual'); 
				 }
				$userAgent = $request->userAgent();				
				$ip = $_SERVER['REMOTE_ADDR'];
				 $scannedType = Session::get('scanned_type');
				//$segment = $request->segment(1);				
				$dataLog= array();
				$dataLog = [
					'fk_qr_codes_list' =>  $user->id,  
					 'action' =>"Visited Game Manual play page",
					 'scanned_type' => $scannedType,
					'page_name' =>  "Manual play page",			
					'ip_address' =>$ip,
					'user_agent' => $userAgent,
					'created_at'=> $todaysDateTime				
				];
				$id = DataLogging::create($dataLog)->id;
				return response(['message' => "", 'status' => 'success']);
			}				
		}
		else
		{ 
			$message='Invalid QR code. For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour errorMsg">admin@rieves.co.uk</a>';									
			return response(['message' => $message, 'status' => 'fail']);
		}	
	}
	
	public function checkQrcodeUsedStatus(Request $request) {	
		$user = auth()->guard('ScannedQrCodes')->user();
		if($user){			
			return redirect()->to('scratch-card');
		}
		else{
			return redirect()->to('not-authorized');
		}
	}
	
	/*check game status on click of browser back button*/
	public function gameStatus(){
		$id = Auth::guard('ScannedQrCodes')->user()->id;			
			$result = gameOutcomes::where('fk_qr_codes_list',''.$id.'')
			->where('game_status',1)						
			->get()->first();
			if(isset($result) && $result->id>0) {				
					$message = 'You have already played the game.  <br>For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>';									
				return response(['message'=>$message, 'status' => 'fail']);
			}
			else{
				return response(['message'=>"",'status' => 'success']);
			}			
	}	
	
	public function scratchCard() 
	{
		if(Auth::guard('ScannedQrCodes')->check())
		{
			//session()->forget('card_attempts');
			//session()->forget('game_status');
			$error = array();
			$id = Auth::guard('ScannedQrCodes')->user()->id;			
			$result = gameOutcomes::where('fk_qr_codes_list',''.$id.'')
			->where('game_status',1)						
			->get()->first();
			if(isset($result) && $result->id>0)
			{
				/*$error = ['message'=>'You have already played the game.', 'status'=>'fail'];*/
				$error = ['message'=>'Please scan the QR code and play the game.  <br>For further assistance, please contact us at  <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>', 'status'=>'fail'];				
				return view('frontend.scratch-single', compact('error'));				
			}
			else{					
				return view('frontend.scratch-single');
			}
		}
		else{
		  	 return redirect()->to('not-authorized');   
		 }
	}

	public function scratchCardStatus(Request $request)
    {
		 if(Auth::guard('ScannedQrCodes')->check()) {
			 
			$qrcode = Auth::guard('ScannedQrCodes')->user()->qr_code;
			$gameStatus = 0;
			$scannedType = Session::get('scanned_type');
			$fk_id = Auth::guard('ScannedQrCodes')->user()->id;
			//$qrcodeStatus = 0;
			$predefined_outcome = Auth::guard('ScannedQrCodes')->user()->win_type;
			 $todaysDate = Carbon::now()->format('Y-m-d H:i:s');
			 
			  $checkDate =  Carbon::now()->format('Y-m-d');	
			 $card_attempts = $request->card_attempt;
			  $scratchedImg = $request->scratchedImg;
			 /*if($card_attempts==9){
				 $gameStatus=1;
			 }
			 else{
				$gameStatus=0;
			}*/

			$gameStatus=1;
			$game_attempt = 1;
			$userAgent = $request->userAgent();
			$ip = $_SERVER['REMOTE_ADDR'];			
			$token =  $request->_token;					 
			$insertData = array();
			$insertData = [
				'fk_qr_codes_list' =>  $fk_id,  
				 'scanned_type' =>$scannedType,
				'game_attempts' =>  $game_attempt,
				'card_attempts' =>  $card_attempts,
				'card_scratched' =>  $scratchedImg,					
				'win_type'=>  $predefined_outcome,
				'token_id'=>$token,
				'ip_address' =>$ip,
				'user_agent' => $userAgent,
				'created_at'=>   $todaysDate,
				'game_status'=> $gameStatus
			];
			$id = "";			
			//\DB::enableQueryLog();			
			$resultScanned = DB::table('scanned_qrcodes')
				->select('scanned_qrcodes.*')
				->where('scanned_qrcodes.fk_qr_codes_list', '=', $fk_id)
				->where('scanned_qrcodes.game_status', '=', 1)
				->where('scanned_qrcodes.token_id', '=',$token)
				//->where(DB::raw("(DATE_FORMAT(scanned_qrcodes.created_at,'%Y-%m-%d'))"), "=",$checkDate)
				->get();
						
			//dd(\DB::getQueryLog()); // Show results of log
			//echo '<pre>'; print_r($resultScanned); echo '</pre>';
			if(isset($resultScanned) && count($resultScanned)>0)
			{						
			}
			else
			{
				//\DB::enableQueryLog();									
				$id = ScannedQrCodes::create($insertData)->id;
				//dd(\DB::getQueryLog()); // Show results of log
				//$segment = $request->segment(1);				
				$dataLog= array();
				$dataLog = [
					'fk_qr_codes_list' =>  $fk_id,  
					 'action' =>"Scratched ".$scratchedImg. " image",
					 'scanned_type' =>$scannedType,
					'page_name' =>  "Scratch page",			
					'ip_address' =>$ip,
					'user_agent' => $userAgent,
					'created_at'=> $todaysDate				
				];
				$dataLogId = DataLogging::create($dataLog)->id;
			}
			if($id>0)
			{								
				//\DB::enableQueryLog();
				$result = qrcodeslist::find($fk_id);
				//dd(\DB::getQueryLog()); // Show results of log
				$qrcodeUsed = 1;
				$result->update([
					'qrcode_status' =>$qrcodeUsed,					
					'updated_at'=>$todaysDate		
				]); 
				$id = $result->id;
				 
				return response(['attempt' => $card_attempts, 'status' => 'success']);
			}
			else{
				//return redirect('reives-game/'.$qrCode)->with(['message' => 'The data is not added to the system successfully', 'status'=>'fail'] );
				return response(['message' => 'The data is not added to the system successfully. <br>For further assistance, please contact us at  <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>', 'status' => 'fail']);
			}
		 }	   
    }
	
	public function qrcodeStatusUpdate()
    {
		 if(Auth::guard('ScannedQrCodes')->check()) {			 
			 
			$pkId = Auth::guard('ScannedQrCodes')->user()->id;
			$qrCode = Auth::guard('ScannedQrCodes')->user()->qr_code;
			$wintype = Auth::guard('ScannedQrCodes')->user()->win_type;
			$game_no = Auth::guard('ScannedQrCodes')->user()->game_no;
			$scannedType = Session::get('scanned_type');
			$resultGame= gameOutcomes::where('fk_qr_codes_list',''.$pkId.'')->where('game_status',1)->get()->first();
			if(isset($resultGame) && $resultGame->id>0)
			{				
				return response(['message'=>'You have already played the game. <br>For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>', 'status' => 'success', 'wintype' =>$wintype]);		
			}
			else
			{		
				 //DB::enableQueryLog();
				$resultScanned = ScannedQrCodes::where('fk_qr_codes_list',''.$pkId.'')->where('game_status',1)->get()->first();
				//dd(DB::getQueryLog());
				if($resultScanned)
					$resultScannedArr = $resultScanned->toArray();
				else
				$resultScannedArr = array();				
				 if(count($resultScannedArr)>0)
				 {			
					$gameAttempts =  1;				
					//$gameAttempts = ScannedQrCodes::where('fk_qr_codes_list', $pkId)->get()->count(); 						
					$gameOutcomes = gameOutcomes::where('fk_qr_codes_list', $pkId)->get()->first();		
					$userAgent = \Request::server('HTTP_USER_AGENT');						
					$ip = $_SERVER['REMOTE_ADDR'];	
					$todaysDate = Carbon::now()->format('Y-m-d H:i:s');	
					if(isset($gameOutcomes) && $gameOutcomes->id>0)
					{
						$result = gameOutcomes::find($gameOutcomes->id);								
						$resultId = $result->id;
					}
					else
					{				
							$insertedData = array();
							$insertedData = [
									'fk_qr_codes_list' =>  $pkId,
									'game_no' =>  $game_no,
									'game_attempts' => $gameAttempts,							
									'final_result'=>  $wintype,
									'ip_address' =>$ip,
									'user_agent' => $userAgent,
									'created_at'=>   $todaysDate,
									'scanned_type' => $scannedType,
									'game_status' => 1
								];						
							
							//return response(['message' => "QR Code is already used", 'status' => 'fail']);
							$resultId = gameOutcomes::create($insertedData)->id;	
							$dataLog= array();
							if($wintype==1){
								$pageName = "Won page";
								$action = "Won the game";
							}
							else{
								$pageName = "Lose page";
								$action = "Lose the game";
							}
							$dataLog = [
								'fk_qr_codes_list' =>  $pkId,  
								 'action' =>$action,
								 'scanned_type' => $scannedType,
								'page_name' =>  $pageName,			
								'ip_address' =>$ip,
								'user_agent' => $userAgent,
								'created_at'=> $todaysDate				
							];
							$dataLogId = DataLogging::create($dataLog)->id;
										
					}					
					if($resultId>0){
						return response(['message'=>'', 'status' => 'success', 'wintype' =>$wintype]);
					}	
					else{			
						return response(['message'=>'The data is not added successfully. <br>For further assistance, please contact us at  <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>', 'status' => 'fail']);
					}
				 }
				 else
				 {	
					return response(['message'=>'The data is not added successfully', 'status' => 'fail']);					
				}
			}			
		 }
		 else{	
		  	//return redirect('reives-game/'.$qrCode)->with(['message' => 'Sorry, Not authorized to play this game.', 'status'=>'fail'] );
			return redirect()->to('not-authorized');   
		 }	 
			 
	}
	
	public function scratchCardResultPage()
    { 
		if(Auth::guard('ScannedQrCodes')->user()->id!="")
		{
			$qrCode = Auth::guard('ScannedQrCodes')->user()->qr_code;
			$winType = Auth::guard('ScannedQrCodes')->user()->win_type;
			$id = Auth::guard('ScannedQrCodes')->user()->id;
			
			$result = gameOutcomes::where('fk_qr_codes_list',''.$id.'')		
			->where('game_status',1)			
			->limit(1)->get();			
			if(isset($result) && count($result)>0){				
				if($winType==1)
				{
					return view('frontend.win');
				}
				else{
					return view('frontend.lose');
				}
			}
			else{
				return redirect()->to('scratch-card');
			}
		}
		else{
			return redirect()->to('not-authorized');
		}
		
	}
	
	public function monthlyDraw()
    { 
		if(Auth::guard('ScannedQrCodes')->user()->id!="")
		{			 
			$qrCode = Auth::guard('ScannedQrCodes')->user()->qr_code;
			$winType = Auth::guard('ScannedQrCodes')->user()->win_type;
			$id = Auth::guard('ScannedQrCodes')->user()->id;
						
			$result = gameOutcomes::where('fk_qr_codes_list',''.$id.'')			
			->where('game_status',1)			
			->limit(1)->get();			
			if(isset($result) && count($result)>0){
				$monthlyDraw = monthlyDraw::where('fk_qr_codes_list', $id)->get()->first();			
				if(isset($monthlyDraw) && $monthlyDraw->id>0){
					$error = ['message'=>'The Email is already submitted for Draw. <br>For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>', 'status'=>'fail'];									
					return view('frontend.email-opt', compact('error'));
				}
				else{
					return view('frontend.email-opt');	
				}							
			}
			else{
				return redirect()->to('scratch-card');
			}
		}
		else{
			return redirect()->to('not-authorized');
		}
		
	}
	
	public function storeMonthlyDraw(Request $request)
	{
		$email = trim($request->email);
		$winType = Auth::guard('ScannedQrCodes')->user()->win_type;
		$qrcode = Auth::guard('ScannedQrCodes')->user()->qr_code;
		$id = Auth::guard('ScannedQrCodes')->user()->id;
		$prize_amount = Auth::guard('ScannedQrCodes')->user()->prize_amount;		
		$todaysDate = Carbon::now()->format('Y-m-d H:i:s');
		$userAgent = $request->userAgent();
		$ip = $_SERVER['REMOTE_ADDR'];
		$scannedType = Session::get('scanned_type');		
		 $monthlyDraw = monthlyDraw::where('fk_qr_codes_list', $id)->get()->first();			
		if(isset($monthlyDraw) && $monthlyDraw->id>0){
					/*$result = monthlyDraw::find($monthlyDraw->id);
					$todaysDate = Carbon::now()->format('Y-m-d H:i:s');					
					$result->update([						
						'email'=>  $email,
						'ip_address' =>$ip,
						'user_agent' => $userAgent,
						'updated_at'=>   $todaysDate
					]); 
					$resultId = $result->id;*/			
				return response(['message' => 'The Email is already submitted for Draw. <br>For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>', 'status' => 'emailSubmitted']);
			}
			else
			{				
				$insertedData = array();
				$insertedData = [
						'fk_qr_codes_list' => $id,						
						'email'=>  $email,
						'ip_address' =>$ip,
						'user_agent' => $userAgent,
						'created_at'=>   $todaysDate				
				];			
				//return response(['message' => "QR Code is already used", 'status' => 'fail']);
				$resultId = monthlyDraw::create($insertedData)->id;	
				$dataLog= array();							
				$dataLog = [
					'fk_qr_codes_list' =>  $id,  
					'action' =>  "Entered Monthly draw email address",
					'scanned_type' => $scannedType,
					'page_name' =>  "Monthly Draw page",			
					'ip_address' =>$ip,
					'user_agent' => $userAgent,
					'created_at'=> $todaysDate				
				];
				$dataLogId = DataLogging::create($dataLog)->id;
			
			}
		/*if($winType==1)
		{
			$message = "Dear User,"."\n\n";
            $message .= "You have won the Game. Please find the prize details"."\n";
            $message .= "Thanks"."\n";
            $message .= "Morrisons Foundation";
			 Mail::send('frontend.Email.scratch-game', ['price' => $prize_amount, 'qrcode'=>$qrcode], function($message) use($request){
                  $message->to($request->email);
                  $message->subject('Rieves Scratch Card Game details');
                });
		}*/		
		if($resultId!="")
		{			
			$todaysDateMonthYr 	= Carbon::now()->format('Y-m');				
			
			$monthlyDrawStatus = monthlyDrawWinners::where('qr_code',''.$qrcode.'')->where('delete_status',0)->where(DB::raw("(DATE_FORMAT(draw_date,'%Y-%m'))"), "=",$todaysDateMonthYr)->get()->first();
			if($monthlyDrawStatus)
				$resultQrCode = $monthlyDrawStatus->toArray();
			else
				$resultQrCode = array();
			
			if(count($resultQrCode)>0) 
			{
				$message =  "<p class=\"text-center\">Winning number? Email <a class=\"text-decoration-none link-unstyled\" href=\"mailto:admin@rieves.co.uk\">admin@rieves.co.uk</a> to organise payment.</p>";
			}
			else{
				$message =  "Thank You for participating.";	
			}
			return response(['message' => $message, 'status' => 'success']);
		}
		else{
			return response(['message' => 'The data is not added successfully.<br>For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>', 'status' => 'fail']);
		}
	
	}

	public function monthlyDrawResult()
	{
		if(Auth::guard('ScannedQrCodes')->user()->id!="")
		{
			return view('frontend.monthly-draw-result');   
		}
		else{
			return redirect()->to('not-authorized');
		}
	}

	public function qrCodemonthlyDrawCheck(Request $request){
		
		 $QRCode =  trim($request->qrcode);
		  $todaysDate = Carbon::now()->format('Y-m-d');
		  $todaysDateTime = Carbon::now()->format('Y-m-d H:i:s');
		   $qrcodeSession = Auth::guard('ScannedQrCodes')->user()->qr_code;
		  $resultQRCodeCheck = qrcodeslist::where('qr_code',''.$QRCode.'')->get()->first();
			if($resultQRCodeCheck)
				$resultQrCode = $resultQRCodeCheck->toArray();
			else
			$resultQrCode = array();
			if(count($resultQrCode)>0 && ($QRCode==$qrcodeSession)) 
			{	
		  		$todaysDateMonthYr 	= Carbon::now()->format('Y-m');		
				$result = monthlyDrawWinners::where('qr_code',''.$QRCode.'')->where('delete_status',0)->where(DB::raw("(DATE_FORMAT(draw_date,'%Y-%m'))"), "=",$todaysDateMonthYr)->get()->first();
				if($result)
					$resultQrCode = $result->toArray();
				else
					$resultQrCode = array();
				if(count($resultQrCode)>0) 
				{			
					$user = Auth::guard('ScannedQrCodes')->user();
					$userAgent = $request->userAgent();				
					$ip = $_SERVER['REMOTE_ADDR'];
					 $scannedType = Session::get('scanned_type');
						$dataLog= array();
					$dataLog = [
						'fk_qr_codes_list' =>  $user->id,  
							 'action' =>"User is checked MONTHLY DRAW RESULT",
							 'scanned_type' => $scannedType,
							'page_name' =>  "Monthly Draw Result page",			
							'ip_address' =>$ip,
							'user_agent' => $userAgent,
							'created_at'=> $todaysDateTime				
						];
						$id = DataLogging::create($dataLog)->id;
						$message =  "<p class=\"text-center\">Winning number? Email <a class=\"text-decoration-none link-unstyled\" href=\"mailto:admin@rieves.co.uk\">admin@rieves.co.uk</a> to organise payment.</p>";
						
						return response(['message' => $message, 'status' => 'success']);
				}
				else
				{ 
					$message = 'Thank You for participating.';									
					return response(['message' => $message, 'status' => 'success']);
				}				
			}
			else
			{ 
				$message = 'Invalid QRCode. <br>For further assistance, please contact us at <a href="mailto:admin@rieves.co.uk" class="remove_link_colour">admin@rieves.co.uk</a>';									
				return response(['message' => $message, 'status' => 'fail']);
			}
			
	}

	public function getMyIPAddress() 
	{
		// Check if the client's IP address is available in the 'HTTP_CLIENT_IP' header
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'] ?? '';
		} 
		// If not, check if the IP address is available in the 'HTTP_X_FORWARDED_FOR' header
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'] ?? '';
		} 
		// If both headers are not present, fallback to using the 'REMOTE_ADDR' header
		else {
			$ip = $_SERVER['REMOTE_ADDR'] ?? '';
		}
	 
		// The variable $ip now holds the user's IP address
		// Additional code can be added here to process or validate the IP address
	 
		// Return the obtained IP address
		return $ip;
	}
	


}
