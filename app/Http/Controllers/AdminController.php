<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\{Admin, qrcodeslist, gameOutcomes, DataLogging, monthlyDrawWinners};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;
use App\Exports\ExportUsersData;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Validator;
use DataTables;
use Carbon\Carbon;

class AdminController extends Controller
{
    //
    /*public function __construct(Admin $user)
    {
        $this->user = $user;
    }*/

    public function index()
    {
        return view('admin.index');
    }

    public function checkAdminLoginStatus(Request $request){
        $errors = array();
     	//print Hash::make('Qwerty12!');
		$rules = [
			'email' => 'required|max:200|email|exists:admins',
			'password' => 'required|string|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'
		];

		$messages = [
			'email.required' => 'The email is required.',
			'email.email' => 'The email is not valid format.',
			'email.exists' => 'This email is not registered in the system.',
			'password.required' => 'The Password is required.',
			'password.regex' => 'The Password format is invalid.'
		];			
		
       $request->validate($rules, $messages);
		
		$result = Admin::where('email',$request->email)->first();       
        if((Hash::check($request->password, $result->password)) ) {
		  //if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
           Auth::guard('admins')->login($result);
		   //$user = auth()->guard('admin')->user();						
			 //return redirect()->route('admin/dashboard');  
			return redirect()->to('/admin/dashboard');              
		}
        else{
           return back()->withInput()->with('error-message', 'Invalid Password!');           
        }
    }
	
	public function qrcodeList()
	{
		return view('admin.qrcode-list');
	}

    public function logout(){
        Auth::guard('admins')->logout();
        return redirect()->route('admin.login');
    }
	
	/*data table */
	public function exportdataList(Request $request)
    {
     	  $view_data['title'] = '';
        if (!$request->ajax()) {
            return view('admin.exportdata')->with($view_data);
        } 
		else 
		{			
			//\DB::enableQueryLog();					
			$result = qrcodeslist::join('game_outcomes','game_outcomes.fk_qr_codes_list','qr_codes_list.id')
							->leftjoin('monthly_draw','monthly_draw.fk_qr_codes_list','qr_codes_list.id');
							if($request->dateFrom!="" && $request->dateTo!="")
							{
								$dateFrom 	=     Carbon::createFromFormat('d/m/Y', $request->dateFrom)->format('Y-m-d');
								$dateTo 	=     Carbon::createFromFormat('d/m/Y', $request->dateTo)->format('Y-m-d');									
								$result =  	$result->whereDate('game_outcomes.created_at', '>=', $dateFrom);
								$result= 	$result->whereDate('game_outcomes.created_at', '<=', $dateTo);
							}
							if($request->gameNo!=""){
								$result =  	$result->where('qr_codes_list.game_no', '=', trim($request->gameNo));
							}

							if($request->searchbox!=""){
								$result =  	$result->where('qr_codes_list.bar_code', '=', trim($request->searchbox));
							}							
														
							//->whereDate('game_outcomes.created_at', '>=', $dateFrom)
						//->whereDate('game_outcomes.created_at', '<=', $dateTo)
							  						
						$result =  	$result->orderby('qr_codes_list.qr_code','asc');
						$result =  	$result->get(['qr_codes_list.id', 'bar_code', 'prize_amount', 'qr_codes_list.qr_code','qr_codes_list.game_no', 'game_outcomes.created_at',  'game_outcomes.created_at as time', 'game_outcomes.user_agent', 'game_outcomes.ip_address', 'game_outcomes.final_result', 'expiry_date', 'monthly_draw.email']);			
			//dd(DB::getQueryLog());		
		     return Datatables::of($result)
            ->addIndexColumn()
				->editColumn('prize_amount', function ($result) {
                if ($result->prize_amount>0)
                    return "£".$result->prize_amount;                
                else
                    return $result->prize_amount;
            })
			->editColumn('final_result', function ($result) {
                if ($result->final_result === 1)
                    return 'Online Winner';
                elseif ($result->final_result === 2)
                    return 'Ticket Winner';
                else
                    return 'Loser';
            })
			->editColumn('expiry_date', function ($result) {
              $expiry_date 	=  Carbon::createFromFormat('Y-m-d', $result->expiry_date)->format('d/m/Y');
               return $expiry_date;
            })
			->editColumn('created_at', function ($result) {
              $created_date  =  Carbon::createFromFormat('Y-m-d H:i:s', $result->created_at)->format('d/m/Y');
               return $created_date;
            })
			->editColumn('time', function ($result) {
				  $localizedCreatedAt = convertToLocale($result->time);
				$time  = $localizedCreatedAt->format('h:i A');
            //  $time  =  Carbon::createFromFormat('Y-m-d H:i:s', $result->time)->format('h:i:A');
			
			
               return $time;
            })
            /*->addColumn('action', function($row){
                $actions = '<a href="javascript:void(0)" class="btn btn-primary">View</a>';
                return $actions;
            })*/
            //->rawColumns(['action'])
            ->make(true);
        }
    }
	
	
		
	public function auditreport(Request $request)
    {
		if (!$request->ajax()) {
				return view('admin.auditreport');
        } 
		else
		{	
			if(isset($request->searchText) && $request->searchText!=""){
					$searchText = trim($request->searchText);
				$model = DB::table('data_logging')
						->join('qr_codes_list', 'qr_codes_list.id', '=', 'data_logging.fk_qr_codes_list')				
						->where('bar_code', '=',$searchText)					
						->select('data_logging.created_at', 'action','qr_codes_list.qr_code', 'qr_codes_list.bar_code', 'data_logging.ip_address', 'data_logging.user_agent')    
						->get();
			}
			else{
				$model = DB::table('data_logging')
				->join('qr_codes_list', 'qr_codes_list.id', '=', 'data_logging.fk_qr_codes_list')			
				  ->select('data_logging.created_at', 'action','qr_codes_list.qr_code', 'qr_codes_list.bar_code', 'data_logging.ip_address', 'data_logging.user_agent')               
                ->get();
			}
       //dd($model->toArray());
       return DataTables()->of($model)->addIndexColumn()
          ->editColumn('created_at', function ($model) {
              $created_at 	=     Carbon::createFromFormat('Y-m-d H:i:s', $model->created_at)->format('d/m/Y H:i:s');			 
               return $created_at;
            })
            //->rawColumns();
			->make(true);
		}	
	}
	
		
    public function exportQRListInExcel(Request $request)
    {
		if($request->dateFrom!="" && $request->dateTo!="")
		{
			$dateFrom 	=     Carbon::createFromFormat('d/m/Y', $request->dateFrom)->format('Y-m-d');
			$dateTo 	=     Carbon::createFromFormat('d/m/Y', $request->dateTo)->format('Y-m-d');	
		}	
		else{
			$dateFrom 	=  "";
			$dateTo      = "";
		}	
		if($request->gameNo!=""){
			$gameNo      = trim($request->gameNo);
		}
		else{
			$gameNo      =   "";
		}
		
		if($request->searchbox!=""){
			$searchbox =  trim($request->searchbox);
		}
		else{
			$searchbox      =   "";
		}
		
		$fileExt = 'xlsx';
        $exportFormat = \Maatwebsite\Excel\Excel::XLSX;        
        $filename = "export-data-".date('d-m-Y H:i:s').".".$fileExt;
        return Excel::download(new ExportUsersData($dateFrom,$dateTo, $gameNo, $searchbox), $filename, $exportFormat);
    }
	
	public function barCodes(Request $request)
    {
		$searchTerm = $request->searchText;			
		$result = DB::table('qr_codes_list')
            ->select('id','bar_code')		
			//->where('Match(bar_code)', 'Against('.$searchTerm.' IN BOOLEAN MODE)')
			  ->whereRaw("MATCH(bar_code)AGAINST('".$searchTerm."*' IN BOOLEAN MODE)")
            //->groupBy('bar_code')
            ->get()->unique('bar_code');
			
		//print_r($result);
		//dd(result); // Show results of log
		$data = array();	
		if(count($result)>0)
		{
			foreach($result as $key => $record)
			{
				$data['label'] = $record->bar_code;
				$data['value'] = $record->bar_code;
				$data['id'] = $record->id;
				$rowSet[] = $data;				
			}				
		}
		else
		{
			$row['label'] = "No matches found";
			$row['value'] = "";
			$rowSet[] = $row;
		}
		return $rowSet;			
	}
	
	public function dashboard()
	{
		$gameStat = array();
		$statQuery = "SELECT DISTINCT(game_no), (SELECT COUNT(DISTINCT fk_qr_codes_list)  FROM `game_outcomes` WHERE game_outcomes.game_no= qr_codes_list.game_no AND `game_outcomes`.`scanned_type` = 'URL' AND game_status=1  GROUP BY game_outcomes.game_no) as totalScanned,
					(SELECT COUNT(DISTINCT fk_qr_codes_list)  FROM `game_outcomes` WHERE game_outcomes.game_no= qr_codes_list.game_no AND `game_outcomes`.`scanned_type` = 'Manual' AND game_status=1    group by game_outcomes.game_no) as totalManual,
					(SELECT COUNT(DISTINCT fk_qr_codes_list) FROM `game_outcomes`  WHERE  game_outcomes.game_no= qr_codes_list.game_no  		        
						and `final_result`=1   GROUP BY game_no) as winCnt,
					(SELECT COUNT(DISTINCT fk_qr_codes_list) FROM `game_outcomes` WHERE `game_outcomes`.`game_no` = `qr_codes_list`.`game_no` 		        
							AND final_result IN(0, 2) GROUP BY game_no) as lostCnt 		
			FROM qr_codes_list ORDER BY game_no ASC";
		$gameStat	= DB::select($statQuery);
		 return view('admin.dashboard',  compact('gameStat'));	

	}
	
	public function monthlydrawWinners(Request $request){
		
		 if (!$request->ajax()) {
			return view('admin.monthly-draw-winners');
        } 
		else
		{
			$result = DB::table('monthly_draw_winners');
			if(isset($request->searchText) && $request->searchText!="")
			{
				$result =  	$result->where('monthly_draw_winners.qr_code', '=', trim($request->searchText));
			}
			$result =  	$result->where('delete_status', '=',0);
			
			$result =  	$result->orderby('monthly_draw_winners.draw_date','desc');
			$result =  	$result->get(['monthly_draw_winners.id', 'draw_date', 'game_no', 'qr_code']);		
			return Datatables::of($result)
		->addIndexColumn()		
		->editColumn('draw_date', function ($result) {
			$draw_date 	=     Carbon::createFromFormat('Y-m-d', $result->draw_date)->format('d/m/Y');
		   return $draw_date;
		})			
          ->addColumn('action', function($row){
                $actions = '<a href="" class="del_" data-id="'.$row->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>';
                return $actions;
            })
           ->rawColumns(['action'])
            ->make(true);
		}	
		 
	}
		
	
	public function storeMonthlydrawWinners(Request $request)
	{
		$gameNo = trim($request->gameNo);
		$qrcode = trim($request->qrcode);
		$drawDate = $request->drawDate;
		$drawDate = Carbon::createFromFormat('d/m/Y', $drawDate)->format('Y-m-d');
		
		$drawDateCheck = Carbon::createFromFormat('d/m/Y', $request->drawDate)->format('Y-m');
	
		$userId = Auth::guard('admins')->user()->id;	
		$todaysDateTime = Carbon::now()->format('Y-m-d H:i:s');
						
		if(isset($request->action) && $request->action=="modalConfirmation")
		{
				$monthlyDrawWinnersInsert= array();
						$monthlyDrawWinnersInsert = [
							'qr_code' => $qrcode,  
							 'game_no' => $gameNo,
							 'draw_date' => $drawDate,
							'fk_admin_id' =>  $userId,		
							'created_at'=> $todaysDateTime				
						];
						$id = monthlyDrawWinners::create($monthlyDrawWinnersInsert)->id;
					if(isset($id) && $id>0)
						return response(['message' => 'The data is stored successfully', 'status' => 'success']);
					else
					return response(['message' => 'Problem in adding the data', 'status' => 'fail']);	
		}			
		else
		{				
			//\DB::enableQueryLog();			
			$result = qrcodeslist::where('qr_code',''.$qrcode.'')->get()->first();
			
			if($result)
				$resultQrCode = $result->toArray();
			else
			$resultQrCode = array();		
			//dd(\DB::getQueryLog());
			if(count($resultQrCode)>0 && ($resultQrCode['game_no']==$gameNo)) 
			{				 
				$monthlyDrawDateCheck = monthlyDrawWinners::where(DB::raw("(DATE_FORMAT(draw_date,'%Y-%m'))"), "=", $drawDateCheck)->where('delete_status', 0)->get()->first();	
				
				  //DB::enableQueryLog();
				 $monthlyDrawWinners = monthlyDrawWinners::where('qr_code', $qrcode)->where('game_no',$gameNo)->where('delete_status', 0)->get()->first();	
				if(isset($monthlyDrawDateCheck) && $monthlyDrawDateCheck->id>0)
				{					
					return response(['message' => "The draw date is already added for this month", 'status' => 'fail']);
				}					 
				else if(isset($monthlyDrawWinners) && $monthlyDrawWinners->id>0)
				{					
					return response(['message' => "This QR Code is already used for Monthly Draw", 'status' => 'toConfirm']);
				}
				else
				{
					$monthlyDrawWinnersInsert= array();
							$monthlyDrawWinnersInsert = [
								'qr_code' => $qrcode,  
								 'game_no' => $gameNo,
								 'draw_date' => $drawDate,
								'fk_admin_id' =>  $userId,		
								'created_at'=> $todaysDateTime				
							];
							$id = monthlyDrawWinners::create($monthlyDrawWinnersInsert)->id;
						if(isset($id) && $id>0)
							return response(['message' => 'The data is stored successfully', 'status' => 'success']);
						else
						return response(['message' => 'Problem in adding the data', 'status' => 'fail']);	
				}
			}
			else{
				if(count($resultQrCode)>0 && ($resultQrCode['game_no']!=$gameNo))			
					return response(['message' => 'The game number does not match with this QR code.', 'status' => 'fail']);			
				else
					return response(['message' => 'This record doesn\'t exist in the system', 'status' => 'fail']);
			}
		
		}
	
	}
	
	public function deleteMonthlydrawWinners(Request $request)
	{
		$todaysDate = Carbon::now()->format('Y-m-d');
		$todaysDateMonthYr 	= Carbon::now()->format('Y-m');
		$monthlyDrawWinners = monthlyDrawWinners::find($request->id); 
		$qrCode = $monthlyDrawWinners['qr_code'];
		$drawDate = $monthlyDrawWinners['draw_date'];
		$drawDateCheck = Carbon::createFromFormat('Y-m-d', $drawDate)->format('Y-m');		
		$result = qrcodeslist::where('qr_code',''.$qrCode.'')->where('qrcode_status', 1)->get();
		
		/*if(count($result)>0 && ($drawDateCheck==$todaysDateMonthYr))
		{
			return response(['message' => 'This QR code is already claimed as monthly draw for current month.', 'status' => 'fail']);
		}
		else
		{*/		
			$monthlyDrawWinners->update([
				'delete_status' =>1,
				'date_deleted'  => $todaysDate           
			]); 
			return response(['message' => 'The record is deleted successfully', 'status' => 'success']);	
		//}
	}
	
	public function dashboard_org()
	{
		$result = DB::table('qr_codes_list')->get(['game_no'])->unique('game_no');
		$gameStat = array();
		if(count($result)>0)
		{
			foreach($result as $record)
			{
				$ScannedQuery ="SELECT COUNT(fk_qr_codes_list) as totalScanned, game_no
									FROM `game_outcomes`
										INNER JOIN `qr_codes_list` on `game_outcomes`.`fk_qr_codes_list` = `qr_codes_list`.`id` 		        
									WHERE `game_outcomes`.`scanned_type` = 'URL' AND game_status=1 AND qr_codes_list.game_no= ".$record->game_no." GROUP BY game_no";
				//print $ScannedQuery.'<br>';				
				$resultScanned = DB::select($ScannedQuery);	
				if(count($resultScanned)>0){									
					$scannedCnt = $resultScanned[0]->totalScanned;									
				}
				else{
					$scannedCnt =0;
				}
				$gameStat[$record->game_no]['totalScanned'] = 	$scannedCnt;	
				
				$manualPlayQuery ="SELECT COUNT(DISTINCT fk_qr_codes_list) as totalManual, game_no
							FROM `game_outcomes` INNER JOIN `qr_codes_list` on `game_outcomes`.`fk_qr_codes_list` = `qr_codes_list`.`id` 		        
							WHERE`game_outcomes`.`scanned_type` = 'Manual' AND game_status=1 AND qr_codes_list.game_no= ".$record->game_no." GROUP BY game_no";
							$resultManualPlay = DB::select($manualPlayQuery);
							//$manualPlayCnt = $resultManualPlay[0]->totalManual;
				if(count($resultManualPlay)>0){									
					$manualPlayCnt = $resultManualPlay[0]->totalManual;									
				}
				else{
					$manualPlayCnt =0;
				}	
				$gameStat[$record->game_no]['totalManual'] = 	$manualPlayCnt;		
				
				$gameWinQuery = "SELECT COUNT(DISTINCT fk_qr_codes_list) as winCnt, game_no
						FROM `game_outcomes`
							INNER JOIN `qr_codes_list` on `game_outcomes`.`fk_qr_codes_list` = `qr_codes_list`.`id` 		        
						WHERE `final_result`=1  AND qr_codes_list.game_no= ".$record->game_no." GROUP BY game_no";
					$resultGameWin = DB::select($gameWinQuery);								
				if(count($resultGameWin)>0){									
					$gameWinCnt = $resultGameWin[0]->winCnt;									
				}
				else{
					$gameWinCnt =0;
				}	
				$gameStat[$record->game_no]['winCnt'] = 	$gameWinCnt;
				
				$gameLoseQuery = "SELECT COUNT(DISTINCT fk_qr_codes_list) as lostCnt,  game_no
								FROM `game_outcomes` INNER JOIN `qr_codes_list` on `game_outcomes`.`fk_qr_codes_list` = `qr_codes_list`.`id` 		        
							WHERE final_result IN(0, 2) AND qr_codes_list.game_no= ".$record->game_no." GROUP BY game_no";
					$resultGameLose = DB::select($gameLoseQuery);		
										
					if(count($resultGameLose)>0){									
					$gameLoseCnt = $resultGameLose[0]->lostCnt;									
				}
				else{
					$gameLoseCnt =0;
				}	
				$gameStat[$record->game_no]['lostCnt'] = 	$gameLoseCnt;
					
			}					
		}
		 return view('admin.dashboard',  compact('gameStat'));		
	}	
}
