<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Hibit\GeoDetect;

class CountryMiddleware
{
    // set Countries ISO Codes 
	public $allowCountries = ['GB', 'IN', 'FR'];
	
	/**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
       	$geoDetect = new GeoDetect();		
		$visitorIP = $this->getMyIPAddress();
		if(env('ENV_SERVER')=='local'){					
			$countryCode = "IN";
		}
		else
		{
			$country = $geoDetect->getCountry($visitorIP);			
			$countryCode = $country->getIsoCode(); 		
		}	
		//if (in_array($request->ip(), $this->allowCountries)) {
		if ($countryCode!="" && (in_array($countryCode, $this->allowCountries))) {
			return $next($request);			
		}
		else{
			return redirect('restrict-access');
		}		
    }
	
	public function getMyIPAddress() 
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'] ?? '';
		} 		
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'] ?? '';
		}		
		else {
			$ip = $_SERVER['REMOTE_ADDR'] ?? '';
		}	 
		// Return the obtained IP address
		return $ip;
	}	

}
