<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class monthlyDrawWinners extends Model
{
    use HasFactory;
	
	protected $table = 'monthly_draw_winners';
	//public $timestamps = false;
	
	 protected $fillable = [
        'qr_code',     
		'game_no',
		'draw_date',
		'fk_admin_id',
		'delete_status',
		'date_deleted',
		'user_agent',
		'created_at',
		'updated_at'     
    ];
}
