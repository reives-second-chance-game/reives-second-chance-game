<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gameOutcomes extends Model
{
    use HasFactory;
	
	 protected $table = 'game_outcomes';
	 public $timestamps = false;
	
	 protected $fillable = [
        'fk_qr_codes_list',
		 'game_no',
       'game_attempts',
		 'final_result',	
		'ip_address',
		'user_agent',
		'created_at',
		'updated_at',
		'game_status',
		'scanned_type'
       
    ];
	
	
}
