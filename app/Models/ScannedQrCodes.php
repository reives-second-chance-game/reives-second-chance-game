<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ScannedQrCodes extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $guarded = ['id'];
    protected $table = 'scanned_qrcodes';
	 public $timestamps = false;
	
	 protected $fillable = [
        'fk_qr_codes_list',
        'scanned_type',
        'game_attempts',
		 'card_attempts',
		 'card_scratched',
		'game_status',
		'win_type',
		'token_id',
		'ip_address',
		'user_agent',
		'created_at',
		'updated_at'
       
    ];
	
	
	
	
}
