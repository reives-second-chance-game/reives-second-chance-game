<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class qrcodeslist extends Authenticatable
{
    use HasFactory, Notifiable;
 
    protected $guarded = ['id'];
    protected $table = 'qr_codes_list';
    //public $timestamps = false;

    protected $fillable = [
        'game_no',
        'retailer',
        'qr_code',
        'bar_code',
        'win_type',
        'prize_amount',
        'expiry_date',
        'url',
		'qrcode_status'
    ];
}
