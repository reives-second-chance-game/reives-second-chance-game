<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class monthlyDraw extends Model
{
    use HasFactory;
	 protected $table = 'monthly_draw';
	 public $timestamps = false;
	
	 protected $fillable = [
        'fk_qr_codes_list',     
		'email',
		'ip_address',
		'user_agent',
		'created_at',
		'updated_at'
       
    ];
}
