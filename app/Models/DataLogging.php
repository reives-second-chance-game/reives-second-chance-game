<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class DataLogging extends Model
{
    use HasFactory, Notifiable;
	protected $guarded = ['id'];
    protected $table = 'data_logging';
	
	 protected $fillable = [
        'fk_qr_codes_list',
        'action',
		 'scanned_type',
        'page_name',		
		'ip_address',
		'user_agent',
		'created_at',
		'updated_at'       
    ];
}
