<?php
namespace App\Exports;
use App\Models\{qrcodeslist};
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;
use Maatwebsite\Excel\Events\{
        BeforeExport,
        AfterSheet
};

use  DB;

class ExportUsersData implements FromCollection, WithHeadings,  ShouldAutoSize, WithEvents, WithColumnFormatting, WithMapping
{
 	 public function __construct($dateFrom, $dateTo, $gameNo, $search) 
    {
        $this->dateFrom = $dateFrom;
		$this->dateTo = $dateTo;
		$this->gameNo = $gameNo;
		$this->search = $search;		
	}

   /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
       //				
       /* $result = qrcodeslist::join('game_outcomes','game_outcomes.fk_qr_codes_list','qr_codes_list.id')
							->leftjoin('monthly_draw','monthly_draw.fk_qr_codes_list','qr_codes_list.id')
							->whereDate('game_outcomes.created_at', '>=', $this->dateFrom)
						->whereDate('game_outcomes.created_at', '<=', $this->dateTo)
						->orderby('qr_codes_list.qr_code','asc')
						->get(['bar_code', 'prize_amount', 'qr_codes_list.qr_code','game_outcomes.ip_address', 'game_outcomes.final_result' 'expiry_date', 'monthly_draw.email']);*/
						
						
			/*$result = qrcodeslist::join('game_outcomes','game_outcomes.fk_qr_codes_list','qr_codes_list.id')
							->leftjoin('monthly_draw','monthly_draw.fk_qr_codes_list','qr_codes_list.id')
							->whereDate('game_outcomes.created_at', '>=', $this->dateFrom)
						->whereDate('game_outcomes.created_at', '<=', $this->dateTo)
						->select('bar_code', 'prize_amount', 'qr_codes_list.qr_code','game_outcomes.ip_address',
						DB::raw('(CASE WHEN game_outcomes.final_result=1 THEN "Online Winner" WHEN game_outcomes.final_result=2 THEN "Ticket Winner" WHEN game_outcomes.final_result=0 THEN "Loser" END) AS final_result, DATE_FORMAT(expiry_date, "%d/%m/%Y") as expiry_date'), 
						 'monthly_draw.email')						
						->orderby('qr_codes_list.qr_code','asc')
						->get();	*/
		
		//\DB::enableQueryLog();			
		$result = qrcodeslist::join('game_outcomes','game_outcomes.fk_qr_codes_list','qr_codes_list.id')
							->leftjoin('monthly_draw','monthly_draw.fk_qr_codes_list','qr_codes_list.id');
							if($this->dateFrom!="" && $this->dateTo!="")
							{																	
								$result =  	$result->whereDate('game_outcomes.created_at', '>=', $this->dateFrom);
								$result= 	$result->whereDate('game_outcomes.created_at', '<=', $this->dateTo);
							}
							if($this->gameNo!=""){
								$result =  	$result->where('qr_codes_list.game_no', '=', $this->gameNo);
							}
							if($this->search!=""){
								$result =  	$result->where('qr_codes_list.bar_code', '=', $this->search);
							}
														
						$result = 	$result->select('bar_code', 'qr_codes_list.qr_code','qr_codes_list.game_no','game_outcomes.created_at', 'game_outcomes.created_at as time',
						'game_outcomes.user_agent','game_outcomes.ip_address', DB::raw('(CASE WHEN game_outcomes.final_result=1 THEN "Online Winner" WHEN game_outcomes.final_result=2 THEN "Ticket Winner" WHEN game_outcomes.final_result=0 THEN "Loser" END) AS final_result, (CASE WHEN prize_amount>0 THEN CONCAT("£",prize_amount) WHEN prize_amount=0 THEN 0 END) AS prize_amount, DATE_FORMAT(expiry_date, "%d/%m/%Y") as expiry_date'), 
						 'monthly_draw.email');							  						
						$result =  	$result->orderby('qr_codes_list.qr_code','asc');
						$result =  	$result->get();	

				//dd(DB::getQueryLog());	
				
	   return  $result;
    }
	
	/* public function map($result): array
    {
      
	   return [
            //$result->invoice_number,
            //Date::dateTimeToExcel($result->created_at),
            //$invoice->total
			 Date::dateTimeToExcel($result->created_at)			
        ];
    }*/
    
    /*public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_DATE_DDMMYYYY
            //'C' => NumberFormat::FORMAT_CURRENCY_EUR_INTEGER,
        ];
    }*/
	
	
	public function map($reference): array
    {
        return [
            $reference->bar_code,
            $reference->prize_amount,
            $reference->qr_code,
            $reference->game_no,
            //Date::dateTimeToExcel($reference->created_at),				
			Carbon::createFromFormat('Y-m-d H:i:s', $reference->created_at)->format('d/m/Y'),
			//date('d/m/Y', strtotime($reference->created_at)),	
			 convertToLocale($reference->time)->format('h:i A'),
			 $reference->user_agent,
			$reference->ip_address,
			$reference->final_result,
			$reference->expiry_date,
			$reference->email
        ];
    }

    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_DATE_DDMMYYYY
        ];
    }
	
	
    public function headings(): array
    {
        return [
            'Barcode Number',
            'Game Prize',
            'QR Code',
			 'Game No.',		
            'Created Date',
			 'Time',
			 'User Agent',
            'Audit IP Address',
            'Status',
            'Expiry Date',
            'Monthly Draw Email'
        ];
    }
	/**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A:G'; // All headers				
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
				$event->sheet->getStyle('A1:G1')->getAlignment()->setHorizontal('center');
				// multi cols
                  $event->sheet->getStyle('A:G')->getAlignment()->setHorizontal('left');
				   // single col
                  $event->sheet->getStyle('B')->getAlignment()->setHorizontal('center');
            },
        ];
    }
	
}
