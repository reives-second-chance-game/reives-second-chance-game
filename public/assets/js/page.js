 $(document).ready(function(){
		var attempts = 0;
		var wins = 0;
		//var maxAttempts = 100;
		var maxAttempts = 6;
		//$('#money_1 img').css({ 'height': '10px !important;', 'width': '10px !important;' }); 
		var row1_col_1 = false;
		var scratchEvent = false;
 $("#row1_col_1").wScratchPad({
  size: 15, // The size of the brush/scratch.  
  //bg: `Images/Gpay_Card ${num}.jpg`, // Background (image path or hex color).
  bg: `assets/images/1.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
   overlay       : 'none',  
  'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {	 
  console.log("percent" + percent);			   
		if (percent>=70)
        {
			 if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row1_col_1 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row1_col_1 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
			
			 if (!row1_col_1) 
			 { 
				row1_col_1 = true;
				var scratchedImg = "First";
				if($('.attempts').html() ==0)
				{	
					attempts = attempts + 1;					
				}
				else{
					attempts = parseInt($('.attempts').html()) + 1;
				}
				scratchEvent = true;
				display_attempts(attempts, scratchEvent,scratchedImg); 			
			}
			
		}
        if (percent >=100)
        {          
		 this.clear();
            //window.alert("U heeft uw code gekrast");
           // window.location.href='compleet.php';
        }
		
		
      },
	scratchDown: function(e, percent){ 
		//console.log("scratchDown " + percent); return false; $(document).off(".scratch"); 
	},
	scratchUp: function(e, percent){		
		return false; 		
		$(document).off(".scratch");  // Remove the scratch event handler 	
		
	}
  
});


$('#row1_col_1 > img').attr('style', 'width: auto; height: auto; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

var row1_col_2 = false;

$("#row1_col_2").wScratchPad({
  size: 15, // The size of the brush/scratch.
  //bg: `Images/Gpay_Card ${num}.jpg`, // Background (image path or hex color). 
   bg: `assets/images/10.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
  'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {
  console.log("percent" + percent);
	 		
       if (percent >=70)
        {
			if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row1_col_2 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row1_col_2 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
			
			if (!row1_col_2) 
			{ 
				row1_col_2 = true;
				var scratchedImg = "Second";				
				if($('.attempts').html() ==0)
				{	
					attempts = attempts + 1;					
				}
				else{
					attempts =  parseInt($('.attempts').html()) + 1;						
				}
				scratchEvent = true;
				display_attempts(attempts, scratchEvent,scratchedImg); 					
			}			
		} 
		if (percent >=100)
        {
            this.clear();
           // window.alert("U heeft uw code gekrast");
           // window.location.href='compleet.php';
        }
		// scratch(); // Call the scratch function on mouse move
		
      },
	scratchDown: function(e, percent){ 
		//console.log("scratchDown " + percent); return false; $(document).off(".scratch"); 
			
	},
	scratchUp: function(e, percent){		
		// Remove the scratch event handler 
		return false; 		
		$(document).off(".scratch");  // Remove the scratch event handler 	
	}
  
});
$('#row1_col_2 > img').attr('style', 'width: auto; height: auto; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

var row1_col_3 = false;
$("#row1_col_3").wScratchPad({
  size: 15, // The size of the brush/scratch.
  //bg: `Images/Gpay_Card ${num}.jpg`, // Background (image path or hex color).
  bg: `assets/images/50.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
  'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {
  console.log("percent" + percent);	 
		if (percent >=70)
        {
			//$('#row1_col_3 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row1_col_3 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row1_col_3 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
			 if (!row1_col_3) 
			 { 
				row1_col_3 = true;	
				var scratchedImg = "Third";
				if($('.attempts').html() ==0)
				{	
					attempts = attempts + 1;					
				}
				else{
					attempts =  parseInt($('.attempts').html()) + 1;						
				}
				scratchEvent = true;
				display_attempts(attempts, scratchEvent,scratchedImg); 					
					
			}
		}
        if (percent >=100)
        {
            this.clear();
            //window.alert("U heeft uw code gekrast");
           // window.location.href='compleet.php';
        }
		// scratch(); // Call the scratch function on mouse move
		
      },
	scratchDown: function(e, percent){ return false; $(document).off(".scratch"); console.log("scratchDown " + percent); },
	scratchUp: function(e, percent){	
		return false; 
		$(document).off(".scratch"); 	
	}
  
});
$('#row1_col_3 > img').attr('style', 'width: 66px; height: 77px; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

//Row2
var row2_col_1 = false;
$("#row2_col_1").wScratchPad({
  size: 15, // The size of the brush/scratch.  
  //bg: `Images/Gpay_Card ${num}.jpg`, // Background (image path or hex color). 
   bg: `assets/images/2.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
   overlay       : 'none',  
  'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {
  console.log("percent" + percent);	 
		if (percent >=70)
        {
			//$('#row2_col_1 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row2_col_1 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row2_col_1 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
			if (!row2_col_1) 
			{ 
				row2_col_1 = true;	
				var scratchedImg = "Fourth";
				if($('.attempts').html() ==0)
				{	
					attempts = attempts + 1;					
				}
				else{
					attempts =  parseInt($('.attempts').html()) + 1;						
				}
				scratchEvent = true;
				display_attempts(attempts, scratchEvent,scratchedImg); 				
			}
		}
        if (percent >=100)
        {          
		 this.clear();
            //window.alert("U heeft uw code gekrast");
           // window.location.href='compleet.php';
        }
		// scratch(); // Call the scratch function on mouse move
		
      },
	scratchDown: function(e, percent){ return false; $(document).off(".scratch"); },
	scratchUp: function(e, percent)
	{  		
		return false; $(document).off(".scratch"); 
		console.log("scratchUp "+ percent); // Remove the scratch event handler 
	
	}
  
});

$('#row2_col_1 > img').attr('style', 'width: 34px; height: 23px; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

var row2_col_2 = false;	
$("#row2_col_2").wScratchPad({
  size: 15, // The size of the brush/scratch.  
  //bg: `images/Gpay_Card ${num}.jpg`, // Background (image path or hex color). 
  bg: `assets/images/1.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
   overlay       : 'none',  
   'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {
  console.log("percent" + percent);	 
		if (percent >=70)
        {
			//$('#row2_col_2 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;');
			if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row2_col_2 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row2_col_2 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
				if (!row2_col_2) 
				{ 
					row2_col_2 = true;
					var scratchedImg = "Fifth";					
					if($('.attempts').html() ==0)
					{	
						attempts = attempts + 1;					
					}
					else{
						attempts =  parseInt($('.attempts').html()) + 1;						
					}
					scratchEvent = true;
				display_attempts(attempts, scratchEvent,scratchedImg);  				
				}
		}
        if (percent >=100)
        {          
		 this.clear();
            //window.alert("U heeft uw code gekrast");
           // window.location.href='compleet.php';
        }
		// scratch(); // Call the scratch function on mouse move
		
      },
	scratchDown: function(e, percent){ return false; $(document).off(".scratch"); console.log("scratchDown " + percent); },
	scratchUp: function(e, percent){  
		return false; 
		$(document).off(".scratch"); 	
	
	}
  
});

$('#row2_col_2 > img').attr('style', 'width: 34px; height: 23px; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

var row2_col_3 = false;	
$("#row2_col_3").wScratchPad({
  size: 15, // The size of the brush/scratch.  
  //bg: `images/Gpay_Card ${num}.jpg`, // Background (image path or hex color). 
  bg: `assets/images/10.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
   overlay       : 'none',  
   'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {
  console.log("percent" + percent);
	 
		if (percent >=70)
        {
			//$('#row2_col_3 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important; ' );
			if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row2_col_3 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row2_col_3 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
			if (!row2_col_3) 
			{ 
				row2_col_3 = true;	
				var scratchedImg = "Sixth";
				if($('.attempts').html() ==0)
				{	
					attempts = attempts + 1;					
				}
				else{
					attempts =  parseInt($('.attempts').html()) + 1;						
				}
				scratchEvent = true;
				display_attempts(attempts, scratchEvent,scratchedImg); 				
			}
		}
        if (percent >=100)
        {          
		 this.clear();
            //window.alert("U heeft uw code gekrast");
           // window.location.href='compleet.php';
        }
		// scratch(); // Call the scratch function on mouse move
		
      },
	scratchDown: function(e, percent){ return false; $(document).off(".scratch"); console.log("scratchDown " + percent); },
	scratchUp: function(e, percent){ 		
		$(document).off(".scratch"); 
		console.log("scratchUp "+ percent); // Remove the scratch event handler 		
	}
  
});

$('#row2_col_3 > img').attr('style', 'width: 34px; height: 23px; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

//row3
var row3_col_1 = false;	
$("#row3_col_1").wScratchPad({
  size: 15, // The size of the brush/scratch.  
  bg: `assets/images/2.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
   overlay       : 'none',  
   'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {
  console.log("percent" + percent);	 
			
		if (percent >=70)
        {
			//$('#row3_col_1 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;');
			if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row3_col_1 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row3_col_1 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
			if (!row3_col_1) 
			{ 
				row3_col_1 = true;
				var scratchedImg = "Seventh";				
				if($('.attempts').html() ==0)
				{	
					attempts = attempts + 1;					
				}
				else{
					attempts =  parseInt($('.attempts').html()) + 1;						
				}
				scratchEvent = true;
				display_attempts(attempts, scratchEvent, scratchedImg); 				
			}
		}
        if(percent >=100)
        {          
			this.clear();
            //window.alert("U heeft uw code gekrast");
           // window.location.href='compleet.php';
        }
		// scratch(); // Call the scratch function on mouse move
		
      },
	scratchDown: function(e, percent){ return false; $(document).off(".scratch"); console.log("scratchDown " + percent); },
	scratchUp: function(e, percent){  
	
		return false; 
		$(document).off(".scratch"); 
		
	}
  
});
$('#row3_col_1 > img').attr('style', 'width: 34px; height: 23px; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

var row3_col_2 = false;	
$("#row3_col_2").wScratchPad({
  size: 15, // The size of the brush/scratch.    
   bg: `assets/images/2.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
   overlay       : 'none',  
   'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {
  console.log("percent" + percent);	 
		if (percent >=70)
        {
			//$('#row3_col_2 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;');
			if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row3_col_2 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row3_col_2 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
			if (!row3_col_2) 
			{ 
				row3_col_2 = true;
				var scratchedImg = "Eighth";				
				if($('.attempts').html() ==0)
				{	
					attempts = attempts + 1;					
				}
				else{
					attempts =  parseInt($('.attempts').html()) + 1;						
				}
				scratchEvent = true;
				display_attempts(attempts, scratchEvent,scratchedImg); 				
			}
		}
        if (percent >=100)
        {          
		 this.clear();
            //window.alert("U heeft uw code gekrast");
           // window.location.href='compleet.php';
        }
		// scratch(); // Call the scratch function on mouse move
		
      },
	scratchDown: function(e, percent){ return false; $(document).off(".scratch"); console.log("scratchDown " + percent); },
	scratchUp: function(e, percent){			
		return false; 
		$(document).off(".scratch"); 		
	}
  
});
$('#row3_col_2 > img').attr('style', 'width: 34px; height: 23px; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

var row3_col_3 = false;
$("#row3_col_3").wScratchPad({
  size: 15, // The size of the brush/scratch.  
  //bg: `images/Gpay_Card ${num}.jpg`, // Background (image path or hex color).  
   bg: `assets/images/1.svg`, // Background (image path or hex color).
  fg: `assets/images/money_bag.png`, // Foreground (image path or hex color).
  //cursor: "crosshair", // Set cursor.
   overlay       : 'none',  
  'cursor':  'url("assets/images/cursor2.png") 5 5, default',
  scratchMove: function (e, percent) {
  console.log("percent" + percent); 
		if (percent >=70)
        {
			//$('#row3_col_3 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;');
			if (window.matchMedia('(max-width: 470px)').matches) {
				 $('#row3_col_3 > img').attr('style', 'width: 70px !important; height: 100% !important; position: absolute; display:block !important;' );
			}else{
				$('#row3_col_3 > img').attr('style', 'width: 100% !important; height: 100% !important; position: absolute; display:block !important;' );
			}
			if (!row3_col_3) 
			{ 
				row3_col_3 = true;
				var scratchedImg = "Ninth";				
				if($('.attempts').html() ==0)
				{	
					attempts = attempts + 1;					
				}
				else{
					attempts =  parseInt($('.attempts').html()) + 1;						
				}
				scratchEvent = true;
				display_attempts(attempts, scratchEvent,scratchedImg); 			
			}		
		
		}
        if (percent >=100)
        {          
		 this.clear();
           
        }
		// scratch(); // Call the scratch function on mouse move
		
      },
	scratchDown: function(e, percent){ return false; $(document).off(".scratch"); console.log("scratchDown " + percent); },
	scratchUp: function(e, percent){	
		return false; 
		$(document).off(".scratch"); 
		
	}
  
});
$('#row3_col_3 > img').attr('style', 'width: 34px; height: 23px; position: absolute; margin: auto; top: 0; left: 0; right: 0; bottom: 0;' );

// Function to simulate scratching

function display_attempts(attempts, scratchEvent,scratchedImg){
		$('.attempts').html(attempts);	
		if(scratchEvent==true ){			
			var token = $('meta[name="csrf-token"]').attr('content'); 
                $.ajax({
                    type: "POST", 
                    url: submitURL,
                    data: {
						'card_attempt': attempts,
						'scratchedImg' : scratchedImg,
						_token: token
                    },
                    success: function(response) {						
						if(response.attempt==9){
							//alert(response.attempt);
							$('.proceed-btn-section').removeClass('hide');
						}
                        else{
							$('.proceed-btn-section').addClass('hide');
						}

                    }
                });
		}
			
			//$('.proceed-btn-section').removeClass('hide');
			
		/*}
		else{
				//$('.proceed-btn-section').addClass('hide');
		}*/
}

 /*$(".proceed-btn-custom").click(function(){	

		  window.location.href = postURL;
	}); */	
	
	

});