<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Models\admin;
use App\Models\ScannedQrCodes;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('admin/clear', function() {
   Artisan::call('cache:clear');
   Artisan::call('config:clear');
   Artisan::call('config:cache');   
   Artisan::call('view:clear');
   Artisan::call('optimize:clear');

   return "Cleared!";

});


Auth::routes();

Route::get('not-authorized', function () {
    return view('frontend.not-authorized');
});

Route::get('restrict-access', function () {
    return view('frontend.website-restriction');
});


Route::group(['middleware' => ['allowCountry','ScannedQrCodes']], function () {
Route::get('scratch-card', ['uses' => 'App\Http\Controllers\gameController@scratchCard'])->name('scratch-card');
Route::get('win', ['uses' => 'App\Http\Controllers\gameController@scratchCardResultPage'])->name('win');
Route::get('lose', ['uses' => 'App\Http\Controllers\gameController@scratchCardResultPage'])->name('lose');
Route::get('monthly-draw', ['uses' => 'App\Http\Controllers\gameController@monthlyDraw'])->name('monthly-draw');
Route::get('monthly-draw-result', ['uses' => 'App\Http\Controllers\gameController@monthlyDrawResult'])->name('monthly-draw-result');
Route::get('play', ['uses' => 'App\Http\Controllers\gameController@play'])->name('play');		 
});


Route::middleware(['allowCountry'])->group(function () {

Route::post('check-qrcode', ['uses' => 'App\Http\Controllers\gameController@checkQrcodeUsedStatus'])->name('check-qrcode');
Route::post('scratch-card-status',array('as'=>'scratch-card-status','uses'=>'App\Http\Controllers\gameController@scratchCardStatus')); 

Route::post('qrcode-status',array('as'=>'product-sortable','uses'=>'App\Http\Controllers\gameController@qrcodeStatusUpdate')); 

Route::post('monthlydraw.store',array('as'=>'monthlydraw.store','uses'=>'App\Http\Controllers\gameController@storeMonthlyDraw')); 

Route::post('qrCodeManualCheck', [App\Http\Controllers\gameController::class, 'qrCodeManualCheck'])->name('qrCodeManual.check'); 

Route::post('gameStatus', [App\Http\Controllers\gameController::class, 'gameStatus']); 

Route::post('qrCodemonthlyDraw.check', [App\Http\Controllers\gameController::class, 'qrCodemonthlyDrawCheck'])->name('qrCodemonthlyDraw.check'); 

});


/*Route::group(['auth:sanctum', 'verified'], function () {
	Route::get('/{qrcode}', [App\Http\Controllers\gameController::class, 'index']);
});*/

//Route::get('/', [App\Http\Controllers\gameController::class, 'index']);
Route::get('admin/', function () {
  return redirect('/admin/login');
});


Route::middleware(['allowCountry'])->group(function () {
	Route::get('/{qrcode}', [App\Http\Controllers\gameController::class, 'index']);
});

Route::middleware(['allowCountry'])->group(function () {
   Route::get('/', [App\Http\Controllers\gameController::class, 'index']);
});


Route::middleware(['allowCountry'])->group(function () {
	Route::get('admin/login',[AdminController::class,'index'])->name('admin.login');
});

Route::middleware(['allowCountry'])->group(function () {

Route::post('admin/checkAdminLoginStatus/',[AdminController::class,'checkAdminLoginStatus'])->name('admin.checkAdminLoginStatus');

Route::get('admin/forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post'); 
Route::get('admin/reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.update');

});

Route::group(['middleware'=>'admin'],function()
{
	 Route::get('/admin/qrcode-list', ['uses' => 'App\Http\Controllers\AdminController@qrcodeList'])->name('qrcode-list');
	Route::post('/uploadCSV', [App\Http\Controllers\csvController::class, 'uploadCSV'])->name('uploadCSV');
	Route::get('/admin/logout',[AdminController::class,'logout'])->name('admin.logout');  
	Route::get('/admin/exportdata', [AdminController::class, 'exportdataList'])->name('exportdata');
	Route::get('/admin/exportQRList', [AdminController::class, 'exportQRListInExcel']);
	Route::get('/admin/auditreport', [AdminController::class, 'auditreport'])->name('auditreport');
	Route::get('/admin/barCodes', [AdminController::class, 'barCodes'])->name('barCodes');
	Route::get('/admin/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
	Route::get('/admin/monthly-draw-winners', [AdminController::class, 'monthlydrawWinners'])->name('monthly-draw-winners');
	Route::post('monthlydrawWinners.store',array('as'=>'monthlydrawWinners.store','uses'=>'App\Http\Controllers\AdminController@storeMonthlydrawWinners')); 
	Route::post('monthlydrawWinnersConfirm.store',array('as'=>'monthlydrawWinnersConfirm.store','uses'=>'App\Http\Controllers\AdminController@storeMonthlydrawWinners')); 
	Route::post('monthlydrawWinners.delete',array('as'=>'monthlydrawWinners.delete','uses'=>'App\Http\Controllers\AdminController@deleteMonthlydrawWinners'));
		
});



